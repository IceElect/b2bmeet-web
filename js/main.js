//Search---------------------------------------------------------------

$(function () {

    $(document).ready(function () {
        $(".header__search").click(function () {
            $(".search__block").toggle('d-block');
            $(".search__ready").toggle('d-block');
        });
    });
});

window.onload = function () {
    var btn = document.querySelector('.header__search');
    var searchWrap = document.querySelector('.search__wrap');
    btn.addEventListener('click', function (e) {
        e.preventDefault();
        if (searchWrap.style.display == 'none') {
            searchWrap.style.display = 'flex';
        } else if (searchWrap.style.display == 'flex') {
            searchWrap.style.display = 'none';
        }
    })
};

//Modal windows---------------------------------------------------------

$(function(){

    $(".reg__form").not(":first").hide();
$(".reg__data .reg__item").click(function() {
    $(".reg__data .reg__item").removeClass("reg__active").eq($(this).index()).addClass("reg__active");
    $(".reg__form").hide().eq($(this).index()).fadeIn()
}).eq(0).addClass("reg__active");

    $('.offer__reg-btn, .offer__btn, .specials__btn, .credits__btn, .header__registration, .side-bar__btn, .side-bar__btn_wind1').click(function (e) {
        e.preventDefault();
        $('#wind1').arcticmodal();
    });

    $('.footer__link_wind2').click(function (e) {
        e.preventDefault();
        $('#wind2').arcticmodal();
    });

    $('.footer__link_wind3').click(function (e){
        e.preventDefault();
        $('#wind3').arcticmodal();
    });

    $('.footer__link_wind4').click(function (e) {
        e.preventDefault();
        $('#wind4').arcticmodal();
    });

    $('.footer__link_wind5').click(function (e) {
        e.preventDefault();
        $('#wind5').arcticmodal();
    });

    $('.footer__link_wind6').click(function (e) {
        e.preventDefault();
        $('#wind6').arcticmodal();
    });

    $('.footer__link_wind7, .side-bar__link_wind7').click(function (e) {
        e.preventDefault();
        $('#wind7').arcticmodal();
    });

    $('.header__link_wind8'). click(function (e) {
        e.preventDefault();
        $('#wind8').arcticmodal();
    });

    $('.side-bar__link_first').click(function (e) {
        e.preventDefault();
        $('#wind9').arcticmodal();
    });

    $('.side-bar__link_second').click(function (e) {
        e.preventDefault ();
        $('#wind10').arcticmodal();
    });

    $('.side-bar__link_third').click(function (e) {
        e.preventDefault ();
        $('#wind11').arcticmodal ();
    });

    $('.header__link_wind12').click(function (e) {
        e.preventDefault();
        $('#wind12').arcticmodal();
    });

    $('.header__help_modal13').click(function (e) {
        e.preventDefault();
        $('#wind13').arcticmodal();
    });

    $(document).on('click', '.center__block_link', function (e) {
        e.preventDefault();

        var user_id = $(this).data('user-id');

        $.get('/user/getModal', {user_id: user_id}, function(data){
            data = JSON.parse(data);

            $(".modal14").html(data.html);
            console.log(data.html);
            $('#wind14').arcticmodal();
        })
    });


});
//checkbox согласен ли я с политикой сайта проверка через if
// $(function())
$(document).on('click', '.reg-footer__arrow', function(){

    var first_name = $('.reg__name-enter').val();
    var last_name = $('.reg__surname-enter').val();
    var post = $('.reg__position-enter').val();
    var email = $('.reg__email-enter').val();
    var mob_phone = $('.reg__mobile-enter').val();
    var company = $('.reg__company-enter').val();
    var city = $('.reg__city-enter').val();
    var inn = $('.reg__tax-enter').val();
    var site = $('.reg__site-enter').val();
    var industry = $('.reg__branch-enter').val();
    var meet_price = $('.reg-footer__input').val();

    var data = {
        first_name : first_name,
        last_name : last_name,
        post : post, email : email,
        phone : mob_phone, 
        company : company, 
        city : city, 
        inn : inn, 
        site : site, 
        industry : industry,
        meet_price : meet_price,
        type: "register"
    };
    
    if(!$('.reg-footer__checkbox').prop('checked')){
        alert('Поставьте галочку!');
        return;
    }
    $.ajax({
        method: 'GET',
        url: base_url + '/user/sendCode',
        dataType: "json",
        data : data,
        success: function(data){
            if(data.response){
                $('.adding_code').css('display', 'block');                    
                $('reg-footer__arrow').addClass('adding__code');
                $('#reg').hide();
                $('.reg-footer').hide();
                $('.reg__close').hide();
                $('.code').show();
            }else{
                alert(data.error.error_msg);
            }
        },
    }); 
});
$(document).on('click', '.reg_code_btn', function(){
    var code = $('.adding_code .modal13__input.code').val();
    $.ajax({
        data : {
            code : code
        },
        url: base_url + '/user/register',
        dataType:"json",
        success: function(data){
            if(data.response){
                window.location.href ="/user/cabinet";
            }else{
                alert(data.error.error_msg);
            }
        },
    });
});
$(document).on('click', '.resend_code', function(){
    $.ajax({
        data : {
        type: "resend"
        },
        url: base_url + '/user/sendCode',
        dataType:"json",
        success: function(data){
            if(data.response){
                alert('Код отправлен повторно');
                // window.location.href ="/user/cabinet";
            }else{
                alert(data.error.error_msg);
            }
        },
    });
});
$(document).on('click', '.modal13 .modal13__btn', function(){
    if($('.modal13 .modal13__btn').hasClass('sendedCode')) {
    var code = $('.modal13 .modal13__input.code').val();
        $.ajax({
            data : {
               code : code 
            },
            url: base_url + '/user/login',
            dataType:"json",
            success: function(data){
                if(data.response){
                    window.location.href ="/user/cabinet";
                }else{
                    alert(data.error.error_msg);
                }
            },
        })
    }else{
        var first_name = $('.modal13__input#first-name').val();
        var phone = $('.modal13__input#mob_phone').val();
        
        var data = {
            first_name : first_name,
            phone : phone
        };
        $.ajax({
            method:'GET',
            url: base_url + '/user/sendCode',
            dataType:"json",
            data : data,
            success: function(data){
                if(data.response){
                    $('.code').show();
                    $('.modal13 .modal13__btn').addClass('sendedCode');
                }else{
                    alert(data.error.error_msg);
                }
            },
        }); 
    }
});

jQuery(document).ready(function(){
    $('.reg__mobile-enter').mask('+70000000000', {placeholder:"+7__________"});
    $('#mob_phone').mask('+70000000000', {placeholder:"+7__________"});
});

function Search(){
    self.params = {count: 10, offset: 0};

    self.setUser = function(id){
        self.user_id = id;
        self.params.user_id = id;
    }

    self.init = function(add){
        $.get("/user/search?type=meet_blink", self.params, function(data){

            data = JSON.parse(data);

            if(add){
                $(".center__wrapper").append(data.list);
            }else{
                $(".center__wrapper").html(data.list);
            }
        })
        $.get("/user/search?type=meet_no_blink", self.params, function(data){

            data = JSON.parse(data);

            $(".center__wrapper").append(data.list);
        })
        $.get("/user/search?type=no_meets", self.params, function(data){

            data = JSON.parse(data);

            $(".center__wrapper").append(data.list);

            if(data.count < self.params.count){
                $("#load-more").parent().hide();
            }
        })
    }

    self.setFilterSelect = function(param, e, el){
        e.preventDefault();

        var value = $(el).find("option:selected").val();

        if(value == 0 || value == "0"){
            delete self.params[param];
        }else{
            self.params[param] = value;
        }
        self.params.offset = 0;
        self.init(false);
    }

    self.setFilterTags = function(param, e, el){

        var value = $('.tags').val();
        console.log(value);

        if(value == 0 || value == "0"){
            delete self.params[param];
        }else{
            self.params[param] = value;
        }
        self.params.offset = 0;
        self.init(false);
    }

    self.setFilterCheckbox = function(param, e, el){
        var value = ($(el).prop('checked'));

        if(value){
            self.params[param] = true;
        }else{
            delete self.params[param];
        }

        self.params.offset = 0;
        self.init(false);
    }

    self.clearFilter = function(e){
        e.preventDefault();

        $("input.tags").tagit("removeAll");
        $("select.search__item_form").val(0);

        delete self.params;
        self.params = {count: 10, offset: 0, user_id: self.user_id};

        self.init(false);
    }

    self.setHandlers = function(){
        $("#load-more").on('click', function(e){
            e.preventDefault();

            self.params.offset = self.params.offset + 10;

            self.init(true);
        })

        $('.tags').tagit({
            availableTags: tags,
            fieldName: 'tags[]',
            tagLimit: 30,
            allowSpaces: false,
            onTagLimitExceeded: function(e){
                jQuery(this).parent().children('.error').show();
            },
            onTagRemoved: function(e){
                jQuery(this).parent().children('.error').hide();
            },
            onTagAdded: function(e){
                self.setFilterTags('tags', e, this);
            }
        });

        $(".search__field").on('keyup change', function(e){
            e.preventDefault();

            var value = $(this).val();
            self.params.search = value;
            self.params.offset = 0;
            self.init(false);
        })

        $(".form__reset").on('click', function(e){
            e.preventDefault();
            self.clearFilter(e);
        })
    }

    self.setUser(user_id);
    self.init(false);
    self.setHandlers();

    return self;
}

if(cabinet){
    var search = new Search();
}