<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Api_functions
{
    private $CI;
    private $data;

    private $notify;

    private $button_texts = array(
        1 => array('out' => 'ПРИГЛАСИТЬ', 'in' => 'ПРИГЛАСИТЬ'),
        2 => array('out' => 'ОЖИДАНИЕ', 'in' => 'ОЖИДАНИЕ'),
        3 => array('out' => 'ГОТОВ', 'in' => 'ГОТОВ'),
        4 => array('out' => 'ВСТРЕЧА', 'in' => 'ВСТРЕЧА'),
        5 => array('out' => 'ОТКАЗ', 'in' => 'ОТКАЗ'),
    );

    private $statuses = array(
        1 => 'ПРИГЛАСИТЬ',
        2 => 'ОЖИДАНИЕ',
        3 => 'ГОТОВ',
        4 => 'ВСТРЕЧА',
        5 => 'ОТКАЗ',
    );

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->library('Api_notify');

        $this->notify = $this->CI->api_notify;
    }

    function init(){
        $function = $this->CI->input->get('function');

        if($function){
            if(method_exists ($this, $function)){
                $data = $this->$function();
            }else{
                $data = array('error' => array('error_msg' => 'Undefined method: ' . $function));
            }
        }

        if($data){
            $response = json_encode($data);

            echo $response;
        }
    }

    function sendCode(){
        $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

        // $iqsms = file_get_contents("http://api.iqsms.ru/messages/v2/send/?login=sssr78l2208&password=325330&phone=" . $_GET['phone'] . "&text=" . $code );
        $iqsms = "accepted;1111";
        $iqsms_data = explode(';', $iqsms);

        $data = array(
            "response" => array(
                "iqsms" => $iqsms_data[1] . "=" . $iqsms_data[0],
                "code_accepted" => $code,
            )
        );
        return $data;
    }

    function logout(){
        return array(
            'response' => array()
        );
    }

    function login(){

        $this->CI->load->model('user_model');

        $phone = $this->CI->input->get('phone');
        $token = $this->CI->input->get('token');

        if($this->CI->user_model->exists($phone)){
            $user = $this->CI->user_model->getUserByPhone($phone);
            $this->CI->user_model->save_user(array('token' => $token), 'edit', $user->id);
            if(empty($user->tags)){
                $user->tags = (array) array();
            }else{
                $user->tags = json_decode($user->tags);
            }
            $data = array(
                "response" => array(
                    "id" => $user->id,
                    "first_name" => $user->first_name,
                    "second_name" => $user->second_name,
                    "last_name" => $user->last_name,
                    "city" => $user->city,
                    "post" => $user->post,
                    "company" => $user->company,
                    "email" => $user->email,
                    "inn" => $user->inn,
                    "industry" => $user->industry,
                    "meet_price" => $user->meet_price,
                    "mob_phone" => $phone,
                    "site" => $user->site,
                    "tags" => (array) array(),
                    // "token" => $token,
                    "iqsms" => "111111=accepted",
                    "code_accepted" => "1111",
                ),
            );
            // $data = array(
            //     // "first_name" => "test",
            //     // "error" => array(
            //         // "error_msg" => "test",
            //         // "error_message" => "test",
            //     // ),
            //     "response" => array(
            //         "id" => $user->id,
            //         "first_name" => "test",
            //         "second_name" => "test",
            //         "last_name" => "test",
            //         "city" => "test",
            //         "post" => "test",
            //         "company" => "test",
            //         "email" => "test",
            //         "inn" => "test",
            //         "industry" => "test",
            //         "meet_price" => "test",
            //         "mob_phone" => $phone,
            //         "site" => "test",
            //         "tags" => (array) array(),
            //     )
            // );
            $user_id = $user->id;
            // $data2 = array(
            //     "id" => 1,
            //     "first_name" => "test",
            //     "second_name" => "test",
            //     "last_name" => "test",
            //     "city" => "test",
            //     "post" => "test",
            //     "company" => "test",
            //     "email" => "test",
            //     "inn" => "test",
            //     "industry" => "test",
            //     "meet_price" => "test",
            //     "mob_phone" => $phone,
            //     "site" => "test",
            //     "tags" => (array) array(),
            // );
            // $data = array('response' => $data);
        }else{
            $data = array(
                "first_name" => "",
                "second_name" => "",
                "last_name" => "",
                "city" => "",
                "post" => "",
                "company" => "",
                "email" => "",
                "inn" => "",
                "industry" => "",
                "meet_price" => "",
                "mob_phone" => $phone,
                "site" => "",
                // "token" => $token,
            );
            $id = $this->CI->user_model->save_user($data);
            $data["iqsms"] = "111111=accepted";
            $data["code_accepted"] = "1111";
            $data['tags'] = (array) array();
            if($id){
                $user_id = $id;
                $data['id'] = $id;
                $data = array('response' => $data);
            }else{
                $data = array(
                    'error' => array(
                        'error_msg' => 'database error'
                    )
                );
            }

        }
        // $data = array(
        //     // "first_name" => "test",
        //     // "error" => array(
        //         // "error_msg" => "test",
        //         // "error_message" => "test",
        //     // ),
        //     "response" => array(
        //         "id" => $user_id,
        //         "first_name" => "test",
        //         "second_name" => "test",
        //         "last_name" => "test",
        //         "city" => "test",
        //         "post" => "test",
        //         "company" => "test",
        //         "email" => "test",
        //         "inn" => "test",
        //         "industry" => "test",
        //         "meet_price" => "test",
        //         "mob_phone" => $phone,
        //         "site" => "test",
        //         "tags" => (array) array(),
        //     )
        // );
        // $response = json_encode(array(
        //         // "first_name" => "test",
        //         // "error" => array(
        //             // "error_msg" => "test",
        //             // "error_message" => "test",
        //         // ),
        //         "response" => array(
        //             "id" => "test",
        //             "first_name" => "test",
        //             "second_name" => "test",
        //             "last_name" => "test",
        //             "city" => "test",
        //             "post" => "test",
        //             "company" => "test",
        //             "email" => "test",
        //             "inn" => "test",
        //             "industry" => "test",
        //             "meet_price" => "test",
        //             "mob_phone" => $phone,
        //             "site" => "test",
        //             "tags" => (array) array(),

        //         )
        //     ));
        return $data;
    }

    function userEdit(){
        $this->CI->load->model('user_model');

        $this->CI->load->model('default_model', 'company');
        $this->CI->company->setTable('company');

        $this->CI->load->model('default_model', 'posts');
        $this->CI->posts->setTable('posts');

        $this->CI->load->model('default_model', 'industries');
        $this->CI->industries->setTable('industries');

        $this->CI->load->model('default_model', 'cities');
        $this->CI->cities->setTable('cities');

        $id = $this->CI->input->get('id');

        $company = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('company')));
        $post = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('post')));
        $company = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('company')));
        $industries = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('industry')));

        $tags = array();
        $aTags = explode(',', $this->CI->input->get('tags'));
        foreach($aTags as $tag){
            $tags[] = (object) array('id' => 1, 'name' => $tag);
        }

        if(count($company)){
            $company = $company[0];

            $data = array(
                "id" => $id,
                "first_name" => $this->CI->input->get('first_name'),
                "second_name" => $this->CI->input->get('second_name'),
                "last_name" => $this->CI->input->get('last_name'),
                "city" => $this->CI->input->get('city'),
                "post" => $this->CI->input->get('post'),
                "company" => $this->CI->input->get('company'),
                "email" => $this->CI->input->get('email'),
                "industry" => $this->CI->input->get('industry'),
                "meet_price" => $this->CI->input->get('meet_price'),
                "mob_phone" => $this->CI->input->get('mob_phone'),
                "site" => $this->CI->input->get('site'),
                "tags" => json_encode($tags),
                "inn" => $company->inn,
            );

            $result = $this->CI->user_model->save($data, 'edit', $id);
            if($result){
                return array(
                    'response' => $data
                );
            }else{
                return array(
                    'error' => array(
                        'error_msg' => 'Database error'
                    )
                );
            }

        }else{
            return array(
                'error' => array(
                    'error_msg' => 'Undefined Company'
                )
            );
        }
    }

    function addPartner(){
        $this->CI->load->model('user_model');

        $this->CI->load->model('default_model', 'company');
        $this->CI->company->setTable('company');

        $this->CI->load->model('default_model', 'posts');
        $this->CI->posts->setTable('posts');

        $this->CI->load->model('default_model', 'industries');
        $this->CI->industries->setTable('industries');

        $this->CI->load->model('default_model', 'cities');
        $this->CI->cities->setTable('cities');

        $user_id = $this->CI->input->get('user_id');

        $company = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('company')));
        $post = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('post')));
        $company = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('company')));
        $industries = $this->CI->company->getDataByWhere(array('name' => $this->CI->input->get('industry')));

        $tags = array();
        $aTags = explode(',', $this->CI->input->get('tags'));
        foreach($aTags as $tag){
            $tags[] = (object) array('id' => 1, 'name' => $tag);
        }

        if(count($company)){
            $company = $company[0];

            $data = array(
                "partner_id" => $user_id,
                "first_name" => $this->CI->input->get('first_name'),
                "second_name" => $this->CI->input->get('second_name'),
                "last_name" => $this->CI->input->get('last_name'),
                "city" => $this->CI->input->get('city'),
                "post" => $this->CI->input->get('post'),
                "company" => $this->CI->input->get('company'),
                "email" => $this->CI->input->get('email'),
                "industry" => $this->CI->input->get('industry'),
                "meet_price" => $this->CI->input->get('meet_price'),
                "mob_phone" => $this->CI->input->get('mob_phone'),
                "site" => $this->CI->input->get('site'),
                "tags" => json_encode($tags),
                "inn" => $company->inn,
            );

            $id = $this->CI->user_model->save($data);
            if($id){
                $data['id'] = $id;
                $data['tags'] = json_decode( $data['tags'] );
                return array(
                    'response' => $data
                );
            }else{
                return array(
                    'error' => array(
                        'error_msg' => 'Database error'
                    )
                );
            }

        }else{
            return array(
                'error' => array(
                    'error_msg' => 'Undefined Company'
                )
            );
        }
    }

    function users(){

        return array('response' => array());

        $this->CI->load->model('user_model');

        $type = $this->CI->input->get('type');
        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $user_id = $this->CI->input->get('user_id');

        $companies_ids = $this->CI->input->get('companies_ids');
        if($companies_ids){
            $this->CI->user_model->filter('company.id IN(' . $companies_ids . ')');
        }

        $posts_ids = $this->CI->input->get('posts_ids');
        if($posts_ids){
            $this->CI->user_model->filter('post.id IN(' . $posts_ids . ')');
        }

        $cities_ids = $this->CI->input->get('cities_ids');
        if($cities_ids){
            $this->CI->user_model->filter('cities.id IN(' . $cities_ids . ')');
        }

        $industries_ids = $this->CI->input->get('industries_ids');
        if($industries_ids){
            $this->CI->user_model->filter('industries.id IN(' . $industries_ids . ')');
        }

        $favorite = $this->CI->input->get('favorite');
        if($favorite){
            $this->CI->user_model->filter('favorites.id > 0');
        }

        // $tags = $this->CI->input->get('industries_ids');
        // if($industries_ids){
        //     $this->CI->user_model->filter('industries.id IN(' . $industries_ids . ')');
        // }

        $statuses_ids = $this->CI->input->get('statuses_ids');
        if($statuses_ids){
            $invite_add = (in_array(1, explode(',', $statuses_ids)))?" OR meets.status_id = ''":'';
            $this->CI->user_model->filter('(meets.status_id IN(' . $statuses_ids . ')' . $invite_add . ')');
        }

        $search = $this->CI->input->get('search');

        $users = array();

        if($type == 'meet_no_blink'){
            $this->CI->user_model->filter("(meets.client_id = ".$user_id." AND meets.user_id != ".$user_id." AND meets.status_id != 3 AND meets.status_id != 1 AND meets.status_id != '')");
            $data = $this->CI->user_model->getUsers($user_id, $search, $count, $offset);

            foreach($data as $user){
                if(empty($user->first_name)) continue;

                $user->rating = $user->meet_rating;
                $user->meet_id = $user->meet_id;
                $user->meet_type = ($user->meet_user_id == $user_id) ? 'in' : 'out';;
                $user->status_id = $user->meet_status;
                $user->status = $this->button_texts[$user->meet_status][$user->meet_type];
                $user->date = $user->meet_date;
                $user->favorite = (!empty($user->is_fav));
                $user->tags = (array) json_decode( $user->tags );

                $user->city_id = intval( $user->city_id );
                $user->post_id = intval( $user->post_id );
                $user->company_id = intval( $user->company_id );
                $user->industry_id = intval( $user->industry_id );

                $users[] = $user;
            }

        }elseif($type == 'meet_blink'){
            $this->CI->user_model->filter("(meets.user_id = ".$user_id." OR (meets.client_id = ".$user_id." AND meets.status_id = 3) AND meets.status_id != 1 AND meets.status_id != '')");
            $data = $this->CI->user_model->getUsers($user_id, $search, $count, $offset);

            foreach($data as $user){
                if(empty($user->first_name)) continue;

                $user->rating = $user->meet_rating;
                $user->meet_id = $user->meet_id;
                $user->meet_type = ($user->meet_user_id == $user_id) ? 'in' : 'out';
                $user->status_id = $user->meet_status;
                $user->status = $this->button_texts[$user->meet_status][$user->meet_type];
                $user->date = $user->meet_date;
                $user->favorite = (!empty($user->is_fav));
                $user->tags = (array) json_decode( $user->tags );

                $user->city_id = intval( $user->city_id );
                $user->post_id = intval( $user->post_id );
                $user->company_id = intval( $user->company_id );
                $user->industry_id = intval( $user->industry_id );

                $users[] = $user;
            }

        }else{
            $this->CI->user_model->filter(" (meets.status_id = '' OR meets.status_id IS NULL OR meets.status_id = 1) ");
            $data = $this->CI->user_model->getUsers($user_id, $search, $count, $offset);

            foreach($data as $user){
                if(empty($user->first_name)) continue;

                $user->rating = 0;
                $user->meet_id = 0;
                $user->meet_type = 'out';
                $user->status_id = 1;
                $user->status = $this->button_texts[1]['out'];
                $user->date = date('Y-m-d');
                $user->favorite = (!empty($user->is_fav));
                $user->tags = (array) json_decode( $user->tags );

                $user->city_id = intval( $user->city_id );
                $user->post_id = intval( $user->post_id );
                $user->company_id = intval( $user->company_id );
                $user->industry_id = intval( $user->industry_id );

                $users[] = $user;
            }
        }

        // echo "<pre>";
        // print_r($users);

        return array('response' => $users);
    }

    function setFavorite(){
        $this->CI->load->model('default_model', 'favorites');
        $this->CI->favorites->setTable('favorites');

        $user_id = $this->CI->input->get('user_id');
        $client_id = $this->CI->input->get('client_id');

        $data = array(
            'user_id' => $user_id,
            'fav_user_id' => $client_id,
            // 'date' => date('Y-m-d H:i:s'),
        );

        if($this->CI->favorites->check_duplicate($data)){
            $result = $this->CI->favorites->delWhere($data);
        }else{
            $result = $this->CI->favorites->save($data);
        }

        if($result){
            return array('response' => (object) array());
        }else{
            return array(
                'error' => array(
                    'error_msg' => 'Database error'
                )
            );
        }
    }

    function deleteFavorite(){
        return $this->setFavorite();
    }

    // Meets

    function sendMeet(){
        $this->CI->load->model('user_model');
        $this->CI->load->model('default_model', 'meet');
        $this->CI->meet->setTable('meets');

        $text = $this->CI->input->get('text');
        $user_id = $this->CI->input->get('user_id');
        $client_id = $this->CI->input->get('client_id');

        $meet_data = array(
            'text' => $text,
            'user_id' => $client_id,
            'client_id' => $user_id,
            'status_id' => 2,
            'date' => date('Y-m-d'),
        );

        $meet_data['id'] = $this->CI->meet->save($meet_data);

        $user = $this->CI->user_model->getUserById($user_id);
        $user->meet_id = $meet_data['id'];
        $user->meet_type = 'in';
        $user->date = date('Y-m-d');
        $user->status_id = $meet_data['status_id'];
        $user->status = $this->button_texts[$meet_data['status_id']][$user->meet_type];
        $user->meet_text = $this->button_texts[$meet_data['status_id']][$user->meet_type];
        $user->favorite = false;
        $user->tags = (array) json_decode( $user->tags );

        $client = $this->CI->user_model->getUserById($client_id);
        $client->meet_id = $meet_data['id'];
        $client->meet_type = 'out';
        $client->date = date('Y-m-d');
        $client->status_id = $meet_data['status_id'];
        $client->status = $this->button_texts[$meet_data['status_id']][$client->meet_type];
        $client->meet_text = $this->button_texts[$meet_data['status_id']][$client->meet_type];
        $client->favorite = false;
        $client->tags = (array) json_decode( $client->tags );

        $this->notify->android_push($client->token, 'С вами хотят встретиться по делу', 'new', $user);

        return array('response' => $client);
    }

    function changeMeet(){
        $this->CI->load->model('user_model');
        $this->CI->load->model('default_model', 'meet');
        $this->CI->meet->setTable('meets');

        $user_id = $this->CI->input->get('user_id');
        $meet_id = $this->CI->input->get('meet_id');
        $status_id = $this->CI->input->get('status_id');

        $meet = $this->CI->meet->getDataById($meet_id);

        $user = $this->CI->user_model->getUserById($user_id);
        $user->rating = $meet['rating'];
        $user->meet_id = $meet['id'];
        $user->meet_type = 'out';
        $user->date = $meet['date'];
        $user->status_id = $status_id;
        $user->status = $this->button_texts[$meet['status_id']][$user->meet_type];
        $user->meet_text = $meet['text'];
        $user->favorite = false;
        $user->tags = (array) json_decode( $user->tags );

        $to_id = ($meet['client_id'] == $user_id) ? $meet['user_id'] : $meet['client_id'];

        $to = $this->CI->user_model->getUserById($to_id);
        $to->meet_id = $meet['id'];
        $to->rating = $meet['rating'];
        $to->meet_type = 'in';
        $to->date = $meet['date'];
        $to->status_id = $status_id;
        $to->status = $this->button_texts[$meet['status_id']][$user->meet_type];
        $to->meet_text = $meet['text'];
        $to->favorite = false;
        $to->tags = (array) json_decode( $to->tags );

        switch($status_id){
            case 3:
                $this->notify->android_push($to->token, 'Ваше приглашение на встречу принято', 'update', $user);
                break;
            case 5:
                $this->notify->android_push($to->token, 'Ваше приглашение на встречу отклонено', 'update', $user);
                break;
        }

        $this->CI->meet->save(array('status_id' => $status_id), 'update', $meet_id);

        return array('response' => $to);
    }

    function meetStatus(){
        $statuses = array();
        $data = array();

        foreach($this->statuses as $id => $status){
            // $data[] = (object) array('id' => $id, 'name' => $status);
        }

        // return array('error' => array('error_msg' => 'sad'));
        // return array('response' => $data);
    }

    function setRating(){
        $this->CI->load->model('meet_model');

        $rating = $this->CI->input->get('rating');
        $meet_id = $this->CI->input->get('meet_id');

        $this->CI->meet_model->save(array('rating' => $rating), 'update', $meet_id);

        return array('response' => array());
    }

    // Pay

    function payResult(){
        $this->CI->load->library('robokassa');

        if($this->CI->robokassa->check_pay($_GET['OutSum'], $_GET['InvId'], $_GET['SignatureValue'])){
            $this->CI->robokassa->register_payment($_GET['InvId']);
            $pId = $this->CI->robokassa->get_order_id($_GET['InvId']);

            $this->CI->load->model('meet_model');
            $meet = $this->meet_model->getMeetByPayment($pId);
            $this->CI->meet_model->save(array('status_id' => 4), 'edit', $meet->id);

            $client = $this->CI->user_model->getUserById($meet->client_id);
            $client->rating = $meet['rating'];
            $client->meet_id = $meet['id'];
            $client->meet_type = 'in';
            $client->date = $meet['date'];
            $client->status_id = 4;
            $client->status = $this->button_texts[4][$user->meet_type];
            $client->meet_text = $meet['text'];
            $client->favorite = false;
            $client->tags = (array) json_decode( $user->tags );

            $user = $this->CI->user_model->getUserById($meet->user_id);
            $user->rating = $meet['rating'];
            $user->meet_id = $meet['id'];
            $user->meet_type = 'out';
            $user->date = $meet['date'];
            $user->status_id = 4;
            $user->status = $this->button_texts[4][$user->meet_type];
            $user->meet_text = $meet['text'];
            $user->favorite = false;
            $user->tags = (array) json_decode( $user->tags );

            $this->notify->android_push($client->token, 'Ваша оплата поступила', 'update', $user);
            $this->notify->android_push($user->token, 'Для вас поступила оплата', 'update', $client);

        }else{
            return array(
                'error' => array(
                    'error_msg' => 'Database error'
                )
            );
        }
    }

    // Posts

    function posts(){
        $this->CI->load->model('default_model', 'posts');
        $this->CI->posts->setTable('posts');

        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $search = $this->CI->input->get('search');

        $this->CI->posts->filter('name LIKE "%' . $search . '%"');
        $data = $this->CI->posts->getAllData($count, $offset);

        return array('response' => $data);
    }

    function addPost(){
        $this->CI->load->model('default_model', 'posts');
        $this->CI->posts->setTable('posts');

        $name = $this->CI->input->get('name');

        $data = array('name' => $name);

        $exists = $this->CI->posts->getDataByWhere($data);
        if( count($exists) > 0 ){
            return array(
                'response' => array(
                    'id' => $exists[0]->id,
                    'name' => $exists[0]->name
                )
            );
        }else{
            $id = $this->CI->posts->save($data);

            if($id){
                $data['id'] = $id;
                return array('response' => $data);
            }else{
                return array(
                    'error' => array(
                        'error_msg' => 'Database error'
                    )
                );
            }
        }

    }

    // Company

    function company(){
        $this->CI->load->model('default_model', 'company');
        $this->CI->company->setTable('company');

        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $search = $this->CI->input->get('search');

        $this->CI->company->filter('name LIKE "%' . $search . '%"');
        $data = $this->CI->company->getAllData($count, $offset);

        return array('response' => $data);
    }

    function addCompany(){
        $this->CI->load->model('default_model', 'company');
        $this->CI->company->setTable('company');

        $inn = $this->CI->input->get('inn');
        $name = $this->CI->input->get('name');

        $data = array('inn' => $inn, 'name' => $name);

        $exists = $this->CI->company->getDataByWhere($data);
        if( count($exists) > 0 ){
            return array(
                'response' => array(
                    'id' => $exists[0]->id,
                    'inn' => $exists[0]->inn,
                    'name' => $exists[0]->name
                )
            );
        }else{
            $id = $this->CI->company->save($data);

            if($id){
                $data['id'] = $id;
                return array('response' => $data);
            }else{
                return array(
                    'error' => array(
                        'error_msg' => 'Database error'
                    )
                );
            }
        }

    }

    // Industries

    function industries(){
        $this->CI->load->model('default_model', 'industries');
        $this->CI->industries->setTable('industries');

        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $search = $this->CI->input->get('search');

        $this->CI->industries->filter('name LIKE "%' . $search . '%"');
        $data = $this->CI->industries->getAllData($count, $offset);

        return array('response' => $data);
    }

    // Cities

    function cities(){
        $this->CI->load->model('default_model', 'cities');
        $this->CI->cities->setTable('cities');

        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $search = $this->CI->input->get('search');

        $this->CI->cities->filter('name LIKE "%' . $search . '%"');
        $data = $this->CI->cities->getAllData($count, $offset);

        return array('response' => $data);
    }

    // Tags

    function tags(){
        $this->CI->load->model('default_model', 'tags');
        $this->CI->tags->setTable('tags');

        $count = $this->CI->input->get('count');
        $offset = $this->CI->input->get('offset');
        $search = $this->CI->input->get('search');

        $this->CI->tags->filter('name LIKE "%' . $search . '%"');
        $data = $this->CI->tags->getAllData($count, $offset);

        return array('response' => $data);
    }

    function addTag(){
        $this->CI->load->model('default_model', 'tags');
        $this->CI->tags->setTable('tags');

        $name = $this->CI->input->get('name');

        $data = array('name' => $name);

        $exists = $this->CI->tags->getDataByWhere($data);
        if( count($exists) > 0 ){
            return array(
                'response' => array(
                    'id' => $exists[0]->id,
                    'name' => $exists[0]->name
                )
            );
        }else{
            $id = $this->CI->tags->save($data);

            if($id){
                $data['id'] = $id;
                return array('response' => $data);
            }else{
                return array(
                    'error' => array(
                        'error_msg' => 'Database error'
                    )
                );
            }
        }

    }

}