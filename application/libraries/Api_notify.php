<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Api_notify
{
    private $CI;

    function __construct()
    {
        $this->CI =& get_instance();
    }

    function android_push($token, $body, $event, $data){
        $socket = @fsockopen('ssl://fcm.googleapis.com', 443, $errno, $errstr, 10);

        if (!$socket)  return false;

        $title = 'B2Bmeet';

          // ---- уведомление для трея ---- //
        $payload = '{
          "to" : "' . $token . '",
          "notification" : {
            "title" : "' . $title . '",
            "body" : "' . $body . '",
            "sound": "default"
          }
        }';
        $payload = array(
            'to' => $token,
            'notification' => array(
                'title' => $title,
                "body" => $body,
                "message" => array('object' => 'meet', 'event' => $event, 'data' => $data),
                "sound" => "default",
                "timestamp" => time()
            ),
            'data' => array(
                'title' => $title,
                "body" => $body,
                "message" => array('object' => 'meet', 'event' => $event, 'data' => $data),
                "sound" => "default",
                "timestamp" => time()
            )
        );
        $payload = json_encode($payload);
        // или
          // ---- уведомление для службы ---- //
        // $payload = '{
        //   "to" : "cGAFgPJGf-s:APA91bF**...**aEVM17c9peqZ",
        //   "data":{
        //     "val1" : "Моё первое сообщение",
        //     "val2" : "(Legacy API) Привет!",
        //     "val3" : "какие-то дополнительные данные"
        //   }
        // }';


        $send  = '';
        $send .= 'POST /fcm/send HTTP/1.1'."\r\n";
        $send .= 'Host: fcm.googleapis.com'."\r\n";
        $send .= 'Connection: close'."\r\n";
        $send .= 'Content-Type: application/json'."\r\n";
        $send .= 'Authorization: key=AIzaSyC92a4gI-nO8h5XpCpRRZOWwWOkPpezdVk'."\r\n";
        $send .= 'Content-Length: '.strlen($payload)."\r\n";
        $send .= "\r\n";

        $send .=$payload;

        $result = fwrite($socket, $send);


        $receive = '';
        while (!feof($socket))  $receive .= fread($socket, 8192);

        fclose($socket);
    }

}