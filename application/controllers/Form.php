<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class Form extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->library('api_notify');
        $this->load->library('robokassa');
        $this->load->model('bill/robokassa_model');
    }

    public function index(){
        
    }

    function import(){
        $result = false;
        $message = "";
        if(isset($_FILES['file'])){
            $this->load->library('excel');

            $file_data = $this->excel->getData($_FILES['file']['tmp_name']);

            // print_r($file_data);

            $data = array();
            $joins = array();

            // echo "<pre>";
            // print_r($file_data['values']);
            foreach ($file_data['values'] as $row) {
                $user = array();

                foreach($file_data['header'][1] as $sim => $field_name){
                    if($field_name == 'id' || $field_name == 'meet_count' || $field_name == 'meet_time' || $field_name == 'type' || $field_name == 'work_phone') continue;

                    if($field_name == 'city_id') $field_name = 'city';
                    if($field_name == 'post_id') $field_name = 'post';
                    if($field_name == 'company_id') $field_name = 'company';
                    if($field_name == 'industry_id') $field_name = 'industry';

                    @$user[$field_name] = $row[$sim];
                }

                // @$user['first_name']  = $row['A'];
                // @$user['second_name'] = $row['B'];
                // @$user['last_name']   = $row['C'];
                // @$user['city']        = $row['D'];
                // @$user['post']        = $row['E'];
                // @$user['company']     = $row['F'];
                // @$user['meet_price']  = $row['G'];
                // @$user['email']       = $row['H'];
                // @$user['mob_phone']   = $row['I'];

                $data[] = $user;
            }

            // echo "<pre>";
            // print_r($data);

            // return;

            $this->load->model('user_model');

            $this->user_model->saveDataFromImport($data);

            echo "<div>Успешно добавлено <b>" . count($data) . "</b> пользователей</div>";

            exit;
        }

        echo '<form method="post" enctype="multipart/form-data"><input type="file" name="file"><input type="submit"></form>';
    }

    function download(){

        $this->load->model('user_model');
        $this->load->library('excel');

        $headers = "first_name, second_name, last_name, city as city_id, industry as industry_id, post as post_id, company as company_id, meet_price, email, site, mob_phone";
        $data = $this->user_model->getDataForExport($headers);

        $keys = array_keys($data[0]);

        array_unshift($data, $keys);

        // echo "<pre>";
        // print_r($data);

        $file = 'b2bmeet-' . time() . '.xls';
        $result = $this->excel->save(FCPATH . '/exports/' . $file, $data);

        echo '<script>window.location.href = "/mobile/exports/'.$file.'";</script>';
        echo "Файл скачивается...";
 
        // if (file_exists($file)) {
        //     // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
        //     // если этого не сделать файл будет читаться в память полностью!
        //     if (ob_get_level()) {
        //         ob_end_clean();
        //     }
        //     // заставляем браузер показать окно сохранения файла
        //     $this->output->set_header('Content-Description: File Transfer');
        //     $this->output->set_header('Content-Type: application/octet-stream');
        //     $this->output->set_header('Content-Disposition: attachment; filename=' . basename($file));
        //     $this->output->set_header('Content-Transfer-Encoding: binary');
        //     $this->output->set_header('Expires: 0');
        //     $this->output->set_header('Cache-Control: must-revalidate');
        //     $this->output->set_header('Pragma: public');
        //     $this->output->set_header('Content-Length: ' . filesize($file));
        //     // читаем файл и отправляем его пользователю
        //     readfile($file);
        //     exit;
        // }
    }

    function pay(){
        $meet_id = $this->input->get('meet_id');
        $meet_price = $this->input->get('meet_price');

        $this->load->model('default_model', 'meet');
        $this->meet->setTable('meets');

        $link = $this->robokassa->create_payment($meet_id, $meet_price, "PCR", "B2Bmeet - встречи по делу, за деньги - оплата информационных услуг") . "&isTest=true";
        $payment_id = $this->robokassa->get_order_id();

        $this->meet->save(array('payment_id' => $payment_id), 'edit', $meet_id);

        echo "<script>document.location.href = '{$link}'</script>";
    }

    function pay_success()
    {
        // if($this->robokassa->check_pay($_GET['OutSum'], $_GET['InvId'], $_GET['SignatureValue'])){
            $this->robokassa->register_payment($_GET['InvId']);

            $this->load->model('user_model');
            $this->load->model('meet_model');
            $meet = (array) $this->meet_model->getMeetByPayment($_GET['InvId']);
            $this->meet_model->save(array('status_id' => 4), 'edit', $meet['id']);

            $client = $this->user_model->getUserById($meet['client_id']);
            $user = $this->user_model->getUserById($meet['user_id']);
            $user->rating = 0;
            $user->meet_id = $meet['id'];
            $user->meet_type = 'out';
            $user->date = $meet['date'];
            $user->status_id = 4;
            $user->status = 'ЗАКАЗ';
            $user->meet_text = $meet['text'];
            $user->favorite = false;
            $user->tags = (array) array();

            $this->api_notify->android_push($client->token, 'Ваша оплата поступила', 'update', $user);
            $this->api_notify->android_push($user->token, 'Для вас поступила оплата', 'update', $client);

        // }else{
            // return array(
                // 'error' => array(
                    // 'error_msg' => 'Database error'
                // )
            // );
        // }
    }
    
    function pay_fail()
    {
        echo "fail";
        print_r($_POST);
    }

}