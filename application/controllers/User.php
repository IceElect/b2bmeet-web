<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');

		$this->load->library('user_lib');
		$this->load->library('api_functions');

		// session_start();
	}

	function index(){
		$this->load->view('index', $this->_getViewData());
	}

	public function error_404(){
        $this->load->view('404', $this->_getViewData());
    }

	public function cabinet(){

		if(!$this->user_lib->is_logged()){
			header('Location: /');
		}

		$data = $this->_getViewData();

		$count = 999;
		$offset = 0;

		$this->load->model('default_model', 'tags');
        $this->tags->setTable('tags');
        $tags = $this->tags->getAllData($count, $offset);

		$this->load->model('default_model', 'posts');
        $this->posts->setTable('posts');
        $posts = $this->posts->getAllData($count, $offset);

        $this->load->model('default_model', 'cities');
        $this->cities->setTable('cities');
        $cities = $this->cities->getAllData($count, $offset);

        $this->load->model('default_model', 'company');
        $this->company->setTable('company');
        $company = $this->company->getAllData($count, $offset);

        $this->load->model('default_model', 'industries');
        $this->industries->setTable('industries');
        $industries = $this->industries->getAllData($count, $offset);

        $data['tags'] = $this->_forSelect($tags);
        $data['posts'] = $posts;
        $data['cities'] = $cities;
        $data['company'] = $company;
        $data['industries'] = $industries;

		$this->load->view('cabinet', $data);
	}

	public function login(){

		if($_SESSION["data_sendCode"]){
			$this->_addToGet($_SESSION["data_sendCode"]);

			$phone = $this->input->get('phone');

			$user = $this->user_model->getUserByPhone($phone);

			if($user){
				$required = array(
					"phone" => 'Номер телефона',
					"first_name" => 'Имя',
				);

				$msg = $this->_checkRequired($required);

				if($this->_validatePhoneNumber($phone)){
					if($msg){
						$this->user_lib->login_with($user->id);

						$data = $this->api_functions->login();
					}else{
						$data = array('error' => array('error_msg' => $msg));
					}
				}else{
					$data = array('error' => array('error_msg' => 'Неверный номер телефона'));
				}
			}else{
				$data = array('error' => array('error_msg' => 'Данный номер телефона не зарегистрирован'));
			}
		}else{
			$data = array('error' => array('error_msg' => 'Ошибка сервера'));
		}

		echo json_encode($data);
	}

	public function register(){

		if($_SESSION["data_sendCode"]){
			$this->_addToGet($_SESSION['data_sendCode']);

			$required = array(
				"phone" => 'Номер телефона',
				"first_name" => 'Имя',
			);

			$msg = $this->_checkRequired($required);

			if($msg === true){
				if($this->_validatePhoneNumber($this->input->get('phone'))){
					if($this->user_model->getUserByPhone($this->input->get('phone'))){
						$data = array('error' => array('error_msg' => 'Пользователь с таким номером уже зарегистрирован'));
						echo json_encode($data);
						return;
					}

					$this->load->model('default_model', 'company');
        			$this->company->setTable('company');

					$company = $this->company->getDataByWhere(array('inn' => $this->input->get('inn')));

					if(!$company){
						$company = (object) array(
							'inn' => $this->input->get('inn'),
							'name' => $this->input->get('company'),
						);
					}

			        $data = array(
		                "first_name" => $this->input->get('first_name'),
		                "second_name" => "",
		                "last_name" => $this->input->get('last_name'),
		                "city" => $this->input->get('city'),
		                "post" => $this->input->get('post'),
		                "company" => $company->name,
		                "email" => $this->input->get('email'),
		                "industry" => $this->input->get('industry'),
		                "meet_price" => $this->input->get('meet_price'),
		                "mob_phone" => $this->input->get('phone'),
		                "site" => $this->input->get('site'),
		                "tags" => json_encode(array()),
		                "inn" => $company->inn,
		            );

					$id = $this->user_model->save_user($data);
		            $data['tags'] = (array) array();
		            $data = array('response' => $data);

		            $this->user_lib->login_with($id);
				}else{
					$data = array('error' => array('error_msg' => 'Неверный номер телефона'));
				}
			}else{
				$data = array('error' => array('error_msg' => $msg));
			}
		}
		echo json_encode($data);
	}

	function sendCode(){

		$msg = true;

		if($this->input->get('type') != "resend"){
			$_SESSION["data_sendCode"] = $_GET;

			$required = array(
				"phone" => 'Номер телефона',
				"first_name" => 'Имя',
			);

			$required_register = array(
				"first_name" => "Имя",
				"last_name" => "Фамилия",
	            "city" => "Город",
	            "post" => "Должность",
	            "company" => "Наименование компании",
	            "email" => "E-mail",
	            "industry" => "Отрасль компании",
	            "meet_price" => "Стоимость встречи",
	            "phone" => "Номер телефона",
	            "site" => "Сайт",
	            "inn" => "ИНН",
			);

			if($this->input->get('type') == "register") $required = $required_register;

			$msg = $this->_checkRequired($required);

		}else{
			if(isset($_SESSION['data_sendCode'])){
				$this->_addToGet($_SESSION['data_sendCode']);
			}else{
				$msg = false;
				$data = array('error' => array('error_msg' => 'Ошибка сервера'));
			}
		}
		
		if($msg === true){
			if($this->_validatePhoneNumber($this->input->get('phone'))){
				$user = $this->user_model->getUserByPhone($this->input->get('phone'));

				if($user || $this->input->get('type') == "register"){
					if($user && $this->input->get('type') == "register"){
						$data = array('error' => array('error_msg' => "Пользователь с таким номером уже зарегистрирован"));
					}else{
						$data = $this->api_functions->sendCode();
					}
				}else{
					$data = array('error' => array('error_msg' => "Данный номер телефона не зарегистрирован"));
				}
			}else{
				$data = array('error' => array('error_msg' => 'Неверный номер телефона'));
			}
		}else{
			$data = array('error' => array('error_msg' => $msg));
		}

		echo json_encode($data);
	}

	function logout(){
		$this->user_lib->logout();
		if(isset($_SERVER['HTTP_REFERER'])) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}else{
			header('Location: /');
		}
	}

	function search(){
		$data = $this->api_functions->users();

		$list = $this->load->view('users_list', array('users' => $data['response']), TRUE);
		$count = count($data['response']);

		echo json_encode(array('list' => $list, 'count' => $count));
	}

	function getModal(){
		$user_id = $this->input->get('user_id');

		$this->load->model('user_model');
		$this->load->model('default_model', 'meet');
        $this->meet->setTable('meets');

		$user = $this->user_model->getUserById($user_id);
		$where = "(client_id = {$this->user_lib->oUser->id} AND user_id = {$user_id}) OR (client_id = {$user_id} AND user_id = {$this->user_lib->oUser->id})";
		$meet = $this->meet->getDataByWhere($where);

		if($meet){ 
			$meet = (array) $meet[0];
		}else{
			$meet = array('status_id' => 1);
		}

		@$user->rating = $meet['rating'];
        @$user->meet_id = $meet['id'];
        @$user->meet_type = 'out';
        @$user->date = $meet['date'];
        @$user->status_id = $status_id;
        @$user->status = $this->api_functions->button_texts[$meet['status_id']][$user->meet_type];
        @$user->meet_price = $this->api_functions->_prePrice($user->id, $user->partner->id);
        @$user->meet_text = $meet['text'];
        @$user->favorite = false;
        @$user->tags = (array) json_decode( $user->tags );

		$html = $this->load->view('meet', array('user' => $user), true);

		echo json_encode(array('html' => $html));
	}

	function _checkRequired($required){

		foreach($required as $field => $field_name){
			if(!isset($_GET[$field])) return "Заполнителе обязательное поле " . $field_name;
			if(empty($_GET[$field])) return "Заполнителе обязательное поле " . $field_name;
		}

		return true;
	}

	function _addToGet($data){
		foreach ($data as $key => $value) {
			$_GET[$key] = $value;
		}
	}

	function _getViewData(){

		$data = array();
		$data['is_logged'] = $this->user_lib->is_logged();
		$data['base_url'] = base_url();
		$data['user'] = $this->user_lib->oUser;

		return $data;
	}

	function _validatePhoneNumber($phone){
	     if (preg_match('/((8|\+7)-?)?\(?\d{3,5}\)?-?\d{1}-?\d{1}-?\d{1}-?\d{1}-?\d{1}((-?\d{1})?-?\d{1})?/', $phone)){
	     	return true;
	     }
	     return false;
	}

	function _forSelect($aData){
		$data = array();
		foreach ($aData as $item) {
			$data[] = $item->name;
		}
		return $data;
	}

}
