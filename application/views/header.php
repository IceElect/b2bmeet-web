<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>B2BMEET</title>
    <meta name="description" content="B2BMEET, встречи">
    <meta name="keywords" content="бизнес, встречи, прибыльб выгоди">
    <!-- Favicon -->
    <!-- OG Tags -->
    <meta property="og:title" content="B2BMEET">
    <meta property="og:description" content="бизнес, встречи, прибыльб выгоди">
    <meta property="og:type" content="article">
    <meta property="og:site_name" content="B2BMEET">
    <!-- font awesome -->
    <!-- <link href="/your-path-to-fontawesome/css/fontawesome.css" rel="stylesheet"> -->
    <!-- <link href="/your-path-to-fontawesome/css/brands.css" rel="stylesheet"> -->
    <!-- <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet"> -->
    <!-- CSS Styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.arcticmodal-0.3.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/media.css">
</head>

<header class="header" id="header">
    <div class="container">
        <div class="header__hat d-flex justify-content-between">
            <a href="/" class="header__logo">
                <img src="<?php echo base_url(); ?>img/logo.png" alt="B2Bmeet">
            </a>
           <?php if(!$is_logged){ ?>
            <a href="#" class="header__help header__help_modal13">
                Войти
            </a>
            <?php } ?>
            <div class="header__left__block d-flex">
                <a href="#" class="header__search d-flex">
                    <i class="fas fa-search"></i>
                    <span class="header__search-word">
                        Поиск
                    </span>
                </a>
           <?php if(!$is_logged){ ?>
                <a href="#" class="header__registration">
                    Регистрация
                </a>
           <?php }?>
           <?php if($is_logged){?>
                <a href="<?php echo base_url(); ?>user/cabinet" class="header__cabinet"> 
                    <?php echo $user->first_name . ' ' . $user->last_name; ?>
                </a>
            <?php }?>
            <?php if($is_logged){ ?>
                <a href="<?php echo base_url(); ?>user/logout" class="header__cabinet">Выйти</a>
            <?php } ?> 
            </div>
        </div>
    </div>
</header>

<script>
    var base_url = '<?php echo base_url(); ?>';
</script>