<?php
include('header.php');
?>
<div class="container-autoheight">
    <div class="container">
        <section class="error">
            <h2>404</h2>
            <p>Страница не найдена!</p>
            <p>Скорее всего, страница, которую вы ищете там</p>
            <button class="back-cabinet"><a href="/">Назад на главную</a></button>
            <p class="operating-system">
                <a href="https://play.google.com/store/apps/details?id=ru.b2bmeet.b2bmeet">
                    <img src="/img/google-play.jpg" alt="Google PLay" class="credits__google-pic">
                </a>
                <a href="https://apps.apple.com/ru/app/b2bmeet/id1276289357">
                    <img src="/img/apple-store.jpg" alt="App Store" class="credits__apple-pic">
                </a>
            </p>
        </section>
    </div>
</div>
<?php
include('footer.php');
?>
</html>