<footer class="footer" id="footer">
    <div class="container">
        <div class="footer__wrapper">
            <div class="footer__block d-flex">
                <a href="#" class="footer__link footer__link_wind6">
                    Обратная связь
                </a>
                <a href="#" class="footer__link footer__link_wind7">
                    Платные услуги
                </a>
                <a href="#" class="footer__link footer__link_wind4">
                    Инвесторам
                </a>
                <a href="#" class="footer__link footer__link_wind2">
                    Товарный знак B2BMEET
                </a>
            </div>
            <div class="footer__block d-flex">
                <a href="#" class="footer__link footer__link_wind5">
                    Условия использования
                </a>
                <a href="#" class="footer__link footer__link_wind5">
                    Правовая информация
                </a>
                <a href="#" class="footer__link footer__link_wind3">
                    Наши вакансии
                </a>
            </div>
            <div class="footer__block footer__block_last d-flex">
                <p class="footer__text">
                    ИП Горбунов А. В. <br>
                    Москва, Б. Академическая 71. <br>
                    ОГРНИП ‎317554300052514 <br>
                    ИНН ‎550201247998
                </p>
                <a href="mailto:info@b2bmeet.ru" class="footer__link">
                    info@b2bmeet
                </a>
            </div>
        </div>
        <p class="footer__copyright">
            &copy; 2019
        </p>
    </div>
</footer>

<link rel="stylesheet" href="<?php echo base_url(); ?>js/tagit/jquery.tagit.css">

<script src="https://kit.fontawesome.com/4b8de92ad7.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.mask.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.arcticmodal-0.3.min.js"></script>
<script src="<?php echo base_url(); ?>js/tagit/tag-it.js"></script>
<script src="<?php echo base_url(); ?>js/main.js"></script>