<div class="modal14__header">
    <div class="window14__container">
        <img src="<?php echo base_url(); ?>img/plain__star.png" alt="star" class="modal14__star">
    </div>
</div>
<div class="modal14__name">
    <div class="window14__container">
    	<div class="modal5__wrap d-flex">
    		<p class="modal15__person" style="flex: 1">
         	<?php echo $user->last_name . ' ' . $user->second_name . '  ' . $user->first_name; ?>
         	</p>
         	<?php if($user->status_id != 2){ ?>
         		<button type="submit" class="modal15__btn"><?=$user->status?></button>
         	<?php } ?>
         </div>
    </div>
</div>
<?php if($user->status_id != 1){ ?>
<div class="modal14__text">
    <div class="window14__container">
        <?=$user->meet_text?>
    </div>
</div>
<?php } ?>
<?php if($user->meet_type == 'in' && $user->status_id == 2){ ?>
<div class="modal14__panel">
    <div class="window14__container">
        <div class="modal14__wrap d-flex">
            <button class="modal14__btn modal14__btn_accept">
                Принять
            </button>
            <button class="modal14__btn modal14__btn_decline">
                Отказать
            </button>
        </div>
    </div>
</div>
<?php } ?>
<div class="modal14__credits d-flex">
    <div class="window14__container">
        <div class="modal14__block d-flex">
            <img src="<?php echo base_url(); ?>img/app/project-manager.png" alt="manager" class="modal14__picture">
            <div class="modal14__intro d-flex">
                <span class="modal14__branch">
                    Должность
                </span>
                <span class="modal14__position">
                    <?=$user->post?>
                </span>
            </div>
        </div>
        <div class="modal14__block d-flex">
            <img src="<?php echo base_url(); ?>img/app/factory.png" alt="company" class="modal14__picture">
            <div class="modal14__intro d-flex">
                <span class="modal14__branch">
                    Компания
                </span>
                <span class="modal14__position">
                    <?=$user->company?>
                </span>
            </div>
        </div>
        <div class="modal14__block d-flex">
            <img src="<?php echo base_url(); ?>img/app/tax.png" alt="tax" class="modal14__picture">
            <div class="modal14__intro d-flex">
                <span class="modal14__branch">
                    ИНН
                </span>
                <span class="modal14__position">
                    <?=$user->inn?>
                </span>
            </div>
        </div>
        <div class="modal14__block d-flex">
            <img src="<?php echo base_url(); ?>img/app/smallmap.png" alt="city" class="modal14__picture">
            <div class="modal14__intro d-flex">
                <span class="modal14__branch">
                    Город
                </span>
                <span class="modal14__position">
                    <?=$user->city?>
                </span>
            </div>
        </div>
        <div class="modal14__block d-flex">
            <img src="<?php echo base_url(); ?>img/app/internet.png" alt="site" class="modal14__picture">
            <div class="modal14__intro d-flex">
                <span class="modal14__branch">
                    Сайт
                </span>
                <span class="modal14__position">
                    <?=$user->site?>
                </span>
            </div>
        </div>
    </div>
</div>