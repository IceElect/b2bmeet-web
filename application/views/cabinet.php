<?php
// var_dump($is_logged);
include('header.php');
?>
<section class="search" id="search">
        <div class="container">
            <form action="#" class="search__wrap" style="display: none">
                <input type="search" maxlength="110" placeholder="Поиск по сайту" class="search__field" />
                <a href="#" class="search__question-mark">
                    <i class="far fa-question-circle"></i>
                </a>
                <button type="submit" class="search__btn btn">
                    Найти
                </button>
            </form>
            <p class="search__ready" style="display: none">
                Найдено 2 841 376 данных у 2 395 505 кандидатов
            </p>
        </div>
    </section>

    <section class="main" id="main">
        <div class="container">
            <div class="main__wrap d-flex justify-content-between">
                <div class="main__filter">
                    <form action="#" class="form d-flex">
                        <div class="form__text d-flex">
                            <h3 class="form__headline">
                                Поиск по фильтру
                            </h3>
                            <a href="#" class="form__reset">
                                сбросить фильтр
                            </a>
                        </div>
                        <ul class="form__menu">
                            <li class="form__item">
                                <div class="form__link d-flex">
                                    <span class="form__undername">
                                        Город
                                    </span>
                                    <select class="search__item search__item_form" onchange="search.setFilterSelect('cities_ids', event, this);">
                                        <option class="search__link" value="0">Не выбрано</option>
                                        <?php 
                                        foreach($cities as $city) {
                                            echo "<option class='search__link' value='$city->id'>$city->name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </li>
                            <li class="form__item">
                                <div class="form__link d-flex">
                                    <span class="form__undername">
                                        Должности
                                    </span>
                                    <select class="search__item search__item_form" onchange="search.setFilterSelect('posts_ids', event, this);">
                                        <option class="search__link" value="0">Не выбрано</option>
                                        <?php 
                                        foreach($posts as $post) {
                                            echo "<option class='search__link' value='$post->id'>$post->name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </li>
                            <li class="form__item">
                                <div class="form__link d-flex">
                                    <span class="form__undername">
                                        Отрасль
                                    </span>
                                    <select class="search__item search__item_form" onchange="search.setFilterSelect('industries_ids', event, this);">
                                        <option class="search__link" value="0">Не выбрано</option>
                                        <?php 
                                        foreach($industries as $work) {
                                            echo "<option class='search__link' value='$work->id'>$work->name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </li>
                            <li class="form__item">
                                <div class="form__link d-flex">
                                    <span class="form__undername">
                                        Теги
                                    </span>
                                    <input type="text" class="form__input tags" />
                                </div>
                            </li>
                            <div class="form__under-menu d-flex justify-content-between">
                                <p class="form__under-text">
                                    Избранное
                                </p>
                                <div class="form__credits d-flex">
                                    <input type="checkbox" name="option" class="form__checkbox" id="favorite_check" onchange="search.setFilterCheckbox('favorite', event, this);" />
                                    <label for="favorite_check">
                                        <span href="#" class="form__star-link">
                                            <img src="<?php echo base_url(); ?>img/plain__star.png" alt="star" class="form__star" />
                                        </span>
                                        <span href="#" class="form__star-link checked">
                                            <img src="<?php echo base_url(); ?>img/red__star.png" alt="star" class="form__star" />
                                        </span>
                                        <!--<span class="form__answer">
                                            Да
                                        </span>-->
                                    </label>
                                </div>
                            </div>
                        </ul>
                    </form>
                    <div class="main__banners">
                        <div class="main__banners-block main__banners-block_extra">
                            <video src="<?php echo base_url(); ?>video/add1.mp4" controls preload="auto" autoplay loop muted></video>
                        </div>
                        <div class="main__banners-block main__banners-block_last">
                            <video src="<?php echo base_url(); ?>video/add2.mp4" controls preload="auto" autoplay loop muted
                                class="<?php echo base_url(); ?>video__main2"></video>
                        </div>
                    </div>
                </div>
                <div class="center">
                    <div class="center__top d-flex">
                        <select class="center__item center__item_form">
                            <option class="center__link">За весь период</option>
                            <option class="center__link">За месяц</option>
                            <option class="center__link">За неделю</option>
                            <option class="center__link">За день</option>
                        </select>
                        <select class="center__item center__item_form">
                            <option class="center__link">По релевантности</option>
                            <option class="center__link">По убыванию</option>
                            <option class="center__link">По возрастанию</option>
                        </select>
                    </div>
                    <div class="center__wrapper"></div>
                    <script>var cabinet = 1;</script>
                    <div class="center__navigation d-flex" style="width: 100%">
                        <!--<div class="center__arrow">
                            &lt;
                        </div>
                        <span class="center__number">
                            1
                        </span>
                        <span class="center__number">
                            2
                        </span>
                        <span class="center__number">
                            3
                        </span>
                        <div class="center__arrow">
                            &gt;
                        </div>-->
                        <a class="btn" id="load-more" style="width: 100%;text-align: center;line-height: 34px;">Показать ещё</a>
                    </div>
                </div>
                <div class="side-bar">
                    <div class="side-bar__achievement">
                        <h2 class="side-bar__headline">
                            Ваши достижения
                        </h2>
                        <div class="side-bar__stars d-flex">
                            <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                            <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                            <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                            <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                            <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                        </div>
                        <p class="side-bar__text">
                            Приглашено -
                            <a href="#" class="side-bar__number">5</a>
                        </p>
                        <p class="side-bar__text">
                            Отказано -
                            <a href="#" class="side-bar__number">5</a>
                        </p>
                        <p class="side-bar__text">
                            Вам готовят ответ -
                            <a href="#" class="side-bar__number">3</a>
                        </p>
                        <p class="side-bar__text">
                            Всего встреч -
                            <a href="#" class="side-bar__number">12</a>
                        </p>
                        <p class="side-bar__text">
                            Хотят с вами встретиться -
                            <a href="#" class="side-bar__number">14</a>
                        </p>
                    </div>
                    <div class="side-bar__services d-flex">
                        <h1>
                            <a href="#" class="side-bar__link side-bar__link_first">
                                Создайте вашу B2Bmeet визитку для соц сетей
                            </a>
                        </h1>
                        <a href="#" class="side-bar__link side-bar__link_second">
                            Доверить организацию встречи B2Bmeet
                        </a>
                        <a href="#" class="side-bar__link side-bar__link_third">
                            Отправить приглашения по отрасли из ТОП-50
                        </a>
                        <a href="#" class="side-bar__link side-bar__link_wind7">
                            Ваш баннер внутри приложения и на сайте
                        </a>
                    </div>
                    <a href="#" class="side-bar__btn side-bar__btn_wind1">
                        Бонус! Добавляй знакомых и получай деньги за его
                        встречи
                    </a>
                    <h3 class="side-bar__title">
                        Самые рейтинговые встречи
                    </h3>
                    <div class="side-bar__names d-flex">
                        <a href="#" class="side-bar__person">
                            Тромс Андрей
                        </a>
                        <a href="#" class="side-bar__person">
                            Шурин Алексей
                        </a>
                        <a href="#" class="side-bar__person">
                            Столяров Алексей
                        </a>
                        <a href="#" class="side-bar__person">
                            Вакатов Артур
                        </a>
                        <a href="#" class="side-bar__person">
                            Иванов Иван
                        </a>
                    </div>
                    <div class="main__banners-block main__banners-block_last">
                        <video src="<?php echo base_url(); ?>video/add2.mp4" controls preload="auto" autoplay loop muted
                            class="<?php echo base_url(); ?>video__main2"></video>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="down-bar d-flex">
        <div class="side-bar__achievement side-bar__achievement_adapt">
            <h2 class="side-bar__headline">
                Ваши достижения
            </h2>
            <div class="side-bar__stars d-flex">
                <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
                <img src="<?php echo base_url(); ?>img/yellow__star.png" alt="star" class="side-bar__star" />
            </div>
            <p class="side-bar__text">
                Приглашено - <a href="#" class="side-bar__number">5</a>
            </p>
            <p class="side-bar__text">
                Отказано - <a href="#" class="side-bar__number">5</a>
            </p>
            <p class="side-bar__text">
                Вам готовят ответ -
                <a href="#" class="side-bar__number">3</a>
            </p>
            <p class="side-bar__text">
                Всего встреч - <a href="#" class="side-bar__number">12</a>
            </p>
            <p class="side-bar__text">
                Хотят с вами встретиться -
                <a href="#" class="side-bar__number">14</a>
            </p>
        </div>
        <div class="side-bar__services side-bar__services_adapt d-flex">
            <h1>
                <a href="#" class="side-bar__link side-bar__link_first">
                    Создайте вашу B2Bmeet визитку для соц сетей
                </a>
            </h1>
            <a href="#" class="side-bar__link side-bar__link_second">
                Доверить организацию встречи B2Bmeet
            </a>
            <a href="#" class="side-bar__link side-bar__link_third">
                Отправить приглашения по отрасли из ТОП-50
            </a>
            <a href="#" class="side-bar__link side-bar__link_wind7">
                Ваш баннер внутри приложения и на сайте
            </a>
        </div>
        <div class="side-bar__extra-block">
            <a href="#" class="side-bar__btn">
                Бонус! Добавляй знакомых и получай деньги за его встречи
            </a>
            <h3 class="side-bar__title">
                Самые рейтинговые встречи
            </h3>
            <div class="side-bar__names d-flex">
                <a href="#" class="side-bar__person">
                    Тромс Андрей
                </a>
                <a href="#" class="side-bar__person">
                    Шурин Алексей
                </a>
                <a href="#" class="side-bar__person">
                    Столяров Алексей
                </a>
                <a href="#" class="side-bar__person">
                    Вакатов Артур
                </a>
                <a href="#" class="side-bar__person">
                    Иванов Иван
                </a>
            </div>
        </div>
    </div>

    <section class="specials" id="specials">
        <div class="container">
            <div class="specials__wrapper d-flex justify-content-between">
                <div class="specials__block">
                    <h2 class="specials__title specials__title_extra">
                        Мы лично организуем встречу для Вас
                    </h2>
                    <p class="specials__text specials__text_extra">
                        Мы готовы взять на себя организацию встречи с
                        представителем интересующей Вас компании. Срок
                        исполнения до 5 рабочих дней Мы готовы взять на себя
                        организацию встречи с представителем интересующей
                        Вас компании. Срок исполнения до 5 рабочих дней
                    </p>
                </div>
                <div class="specials__block">
                    <h2 class="specials__title">
                        Доставим ваше коммерческое предложение
                    </h2>
                    <p class="specials__text">
                        Мы уведомим специальстов интересующей вас отрасли, о
                        важном вашем событии, направим ваше письмо и смс,
                        быстро и качественно Мы уведомим специальстов
                        интересующей вас отрасли, о важном вашем событии,
                        направим ваше письмо и смс, быстро и качественно
                    </p>
                </div>
                <div class="specials__block">
                    <h2 class="specials__title">
                        Получайте деньги за встречи ваших знакомых
                    </h2>
                    <p class="specials__text">
                        Добавляй знакомого руководителя и получай деньги за
                        его встречи. по партнерской программе Добавляй
                        знакомого руководителя и получай деньги за его
                        встречи. по партнерской программе
                    </p>
                    <button class="specials__btn btn">
                        Начать зарабатывать сейчас
                    </button>
                </div>
            </div>
        </div>
    </section>

    <section class="banners" id="banners">
        <div class="container">
            <video src="<?php echo base_url(); ?>video/add3.mp4" class="banners__video3" controls preload="auto" autoplay loop muted></video>
        </div>
    </section>

    <section class="credits" id="credits">
        <div class="container">
            <div class="credits__row-one d-flex justify-content-between">
                <div class="credits__block credits__block_one">
                    <h4 class="credits__headline">
                        Мобильное приложение
                    </h4>
                    <div class="credits__mobile d-flex">
                        <div class="credits__google">
                            <a href="https://play.google.com/store/apps/details?id=ru.b2bmeet.b2bmeet" class="credits__get-on-store">
                                <img src="<?php echo base_url(); ?>img/google-play.jpg" alt="Google PLay" class="credits__google-pic" />
                            </a>
                            <p class="credits__text-extra">
                                Для смартфона на базе <br />
                                Android
                                <sup class="credits__symbol">&reg;</sup>
                                4.0.3 и выше
                            </p>
                        </div>
                        <div class="credits__apple">
                            <a href="https://apps.apple.com/ru/app/b2bmeet/id1276289357" class="credits__get-on-store">
                                <img src="<?php echo base_url(); ?>img/apple-store.jpg" alt="App Store" class="credits__apple-pic" />
                            </a>
                            <p class="credits__text-extra">
                                Для iPhone с IOS 9.0 и выше
                            </p>
                        </div>
                    </div>
                </div>
                <div class="credits__block credits__block_two">
                    <h4 class="credits__headline">
                        Поддержка
                    </h4>
                    <div class="credits__phones">
                        <p class="credits__phone d-flex">
                            <a href="tel:+74951234534" class="credits__tel">
                                +7 495 123-45-34
                            </a>
                            Москва и МО
                        </p>
                        <p class="credits__phone d-flex">
                            <a href="tel:+78121233445" class="credits__tel">
                                +7 812 123-34-45
                            </a>
                            Санкт-Петербург и ЛО
                        </p>
                        <p class="credits__phone d-flex">
                            <a href="tel:88003006567" class="credits__tel credits__tel_last">
                                8 800 300 6567
                            </a>
                            &nbsp;&nbsp;&nbsp;регионы РФ
                        </p>
                    </div>
                </div>
                <div class="credits__block credits__block_three">
                    <h4 class="credits__headline">
                        Мы в социальных сетях
                    </h4>
                    <div class="credit__social d-flex">
                        <div class="credit__social-wrap">
                            <a href="https://vk.com/" target="_blank" class="credits__web">
                                <i class="fab fa-vk credits__icon"></i>
                            </a>
                            <a href="https://www.facebook.com" target="_blank" class="credits__web">
                                <i class="fab fa-facebook-f credits__icon"></i>
                            </a>
                            <a href="https://twitter.com" target="_blank" class="credits__web">
                                <i class="fab fa-twitter credits__icon"></i>
                            </a>
                        </div>
                        <div class="credit__social-wrap2">
                            <a href="https://www.youtube.com/" target="_blank" class="credits__web">
                                <i class="fab fa-youtube credits__icon"></i>
                            </a>
                            <a href="https://www.instagram.com" target="_blank" class="credits__web">
                                <i class="fab fa-instagram credits__icon"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="credits-two" id="credits-two">
        <div class="container">
            <div class="credits__row-two d-flex justify-content-between">
                <div class="credits__wrapper-extra">
                    <div class="offer__wrap">
                        <div class="offer__second-wrap d-flex">
                            <i class="far fa-envelope"></i>&nbsp;
                            <p class="credits-form__text">
                                Получайте новости о встречах по электронной
                                почте:
                            </p>
                        </div>
                        <form action="#" class="offer-form d-flex">
                            <input type="email" placeholder="Введите Ваш E-mail" class="offer__email" />
                            <button class="offer__form-btn btn">
                                Подписаться
                            </button>
                        </form>
                    </div>
                    <div class="offer__terms-wrap d-flex">
                        <p class="offer__terms-text offer__terms-text_add ">
                            Подписываясь на рассылку, Вы соглашаетесь с
                            нашими Положениями и условиями и уведомление о
                            конфиденциальности, а также с использованием
                            файлов cookie
                        </p>
                    </div>
                </div>
                <div class="credits__block credits__block_five">
                    <video src="<?php echo base_url(); ?>video/add1.mp4" class="credits__video1" controls preload="auto" autoplay loop
                        muted></video>
                </div>
                <div class="credits__block credits__block_six">
                    <h3 class="credits__headline credits__headline_add">
                        Создайте вашу B2Bmeet визитную карточку!
                    </h3>
                    <p class="credits__text">
                        Привлекайте клиентов, разместив свою визитную
                        карточку в социальных сетях, в блоге или на сайте, и
                        получайте деньги за встречу с вами
                    </p>
                    <button class="credits__btn btn">
                        Зарегистрироваться
                    </button>
                </div>
            </div>
        </div>
    </section>

<script>
    var tags = <?php echo json_encode($tags); ?>;
    var user_id = <?php echo $user->id; ?>;
</script>
<?php include('footer.php');?>
    
    <!--------------------------Modal windows--------------------------------------------------------->

    <div id="window15" style="display: none">
        <div class="box-modal" id="wind15">
            <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="cross" class="reg__close">
            </div>
            <div class="modal15">
                <div class="modal15__header">
                    <div class="window15__container">
                        <img src="<?php echo base_url(); ?>img/plain__star.png" alt="star" class="modal14__star">
                    </div>
                </div>
                <div class="modal15__name">
                    <div class="window15__container">
                        <div class="modal5__wrap d-flex">
                            <p class="modal15__person">
                                Андрей Тромс <br> Андреевич
                            </p>
                            <button type="submit" class="modal15__btn">
                                Ожидание
                            </button>
                        </div>
                    </div>
                </div>
                <div class="modal15__text">
                    <div class="window15__container">
                        <p class="modal15__service">
                            Добрый день. Давайте встретимся в Вс 17го числа. <br>
                            Нужно обсудить дольнейшее сотрудничество. <br>
                            Звоните в любое время. <br>
                        </p>
                        <div class="modal15__wrap d-flex">
                            <span class="modal15__date">
                                Дата
                            </span>
                            <p class="modal15__date2">
                                24 окт. 2017г.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal15__panel">
                    <div class="window15__container">
                        <div class="modal15__wrap d-flex">
                            <img src="<?php echo base_url(); ?>img/coins.png" alt="" class="modal15__coins">
                            <div class="modal15__intro d-flex">
                                <p class="modal15__meet">
                                    Стоимость встречи
                                </p>
                                <p class="modal5__price">
                                    5 000&#8381;
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal14__credits d-flex">
                    <div class="window14__container">
                        <div class="modal14__block d-flex">
                            <img src="<?php echo base_url(); ?>img/app/project-manager.png" alt="manager" class="modal14__picture">
                            <div class="modal14__intro d-flex">
                                <span class="modal14__branch">
                                    Должность
                                </span>
                                <span class="modal14__position">
                                    Директор
                                </span>
                            </div>
                        </div>
                        <div class="modal14__block d-flex">
                            <img src="<?php echo base_url(); ?>img/app/factory.png" alt="company" class="modal14__picture">
                            <div class="modal14__intro d-flex">
                                <span class="modal14__branch">
                                    Компания
                                </span>
                                <span class="modal14__position">
                                    ООО "Стройинвест"
                                </span>
                            </div>
                        </div>
                        <div class="modal14__block d-flex">
                            <img src="<?php echo base_url(); ?>img/app/tax.png" alt="tax" class="modal14__picture">
                            <div class="modal14__intro d-flex">
                                <span class="modal14__branch">
                                    ИНН
                                </span>
                                <span class="modal14__position">
                                    98784654189851357
                                </span>
                            </div>
                        </div>
                        <div class="modal14__block d-flex">
                            <img src="<?php echo base_url(); ?>img/app/smallmap.png" alt="city" class="modal14__picture">
                            <div class="modal14__intro d-flex">
                                <span class="modal14__branch">
                                    Город
                                </span>
                                <span class="modal14__position">
                                    Москва
                                </span>
                            </div>
                        </div>
                        <div class="modal14__block d-flex">
                            <img src="<?php echo base_url(); ?>img/app/internet.png" alt="site" class="modal14__picture">
                            <div class="modal14__intro d-flex">
                                <span class="modal14__branch">
                                    Сайт
                                </span>
                                <span class="modal14__position">
                                    www.xxxxxxxxxx.com
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window14" style="display: none">
        <div class="box-modal" id="wind14">
            <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="cross" class="reg__close">
            </div>
            <div class="modal14">
                
            </div>
        </div>
    </div>

    <div id="window7" style="display: none">
        <div class="box-modal" id="wind7">
            <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close"></div>
            <div class="window7__container">
                <div class="modal7">
                    <div class="modal7__top-wrap d-flex">
                        <h2 class="modal7__headline">
                            Платные услуги на сайте B2Bmeet
                        </h2>
                        <p class="modal7__title">
                            Привлекайте клиентов среди наших пользователей, все увидят ваше рекламное предложение, а
                            дальнейшее приглашение на встречу, повысит ваши шансы на успех!
                        </p>
                    </div>
                    <div class="modal7__bottom-wrap d-flex">
                        <div class="modal7__banners d-flex">
                            <div class="modal7__wrap-left">
                                <div class="modal7__box d-flex">
                                    <div class="modal7__figure modal7__figure_one"></div>
                                    <div class="modal7__description modal7__description_one">
                                        <div class="modal7__fold d-flex">
                                            <input type="checkbox" class="modal7__checkbox">
                                            <p class="modal7__price">
                                                45 000 руб.
                                            </p>
                                        </div>
                                        <p class="modal7__banner-name">
                                            Баннер малый
                                        </p>
                                        <p class="modal7__banner-time">
                                            120х30 &nbsp;7 дней
                                        </p>
                                    </div>
                                </div>
                                <div class="modal7__box d-flex">
                                    <div class="modal7__figure modal7__figure_two"></div>
                                    <div class="modal7__description">
                                        <div class="modal7__fold d-flex">
                                            <input type="checkbox" class="modal7__checkbox">
                                            <p class="modal7__price">
                                                45 000 руб.
                                            </p>
                                        </div>
                                        <p class="modal7__banner-name">
                                            Баннер средний
                                        </p>
                                        <p class="modal7__banner-time">
                                            240х30 &nbsp;7 дней
                                        </p>
                                    </div>
                                </div>
                                <div class="modal7__box d-flex">
                                    <div class="modal7__figure modal7__figure_three"></div>
                                    <div class="modal7__description modal7__description_three">
                                        <div class="modal7__fold d-flex">
                                            <input type="checkbox" class="modal7__checkbox">
                                            <p class="modal7__price">
                                                45 000 руб.
                                            </p>
                                        </div>
                                        <p class="modal7__banner-name">
                                            Баннер средний
                                        </p>
                                        <p class="modal7__banner-time">
                                            360х30 &nbsp;7 дней
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal7__wrap-right">
                                <div class="modal7__box d-flex">
                                    <div class="modal7__figure modal7__figure_four"></div>
                                    <div class="modal7__description modal7__description_one">
                                        <div class="modal7__fold d-flex">
                                            <input type="checkbox" class="modal7__checkbox">
                                            <p class="modal7__price">
                                                25 000 руб./день
                                            </p>
                                        </div>
                                        <p class="modal7__banner-name">
                                            Тизер на главной <br>странице
                                        </p>
                                    </div>
                                </div>
                                <div class="modal7__box d-flex">
                                    <div class="modal7__figure modal7__figure_five">
                                        <div class="modal7__line1"></div>
                                        <div class="modal7__line2"></div>
                                    </div>
                                    <div class="modal7__description">
                                        <div class="modal7__fold modal7__fold_five d-flex">
                                            <input type="checkbox" class="modal7__checkbox">
                                            <p class="modal7__price">
                                                15 000 руб./день
                                            </p>
                                        </div>
                                        <p class="modal7__banner-name">
                                            Рекламный анонс
                                        </p>
                                    </div>
                                </div>
                                <div class="modal7__box modal7__box_last">
                                    <div class="modal7__fold modal7__fold_five d-flex">
                                        <input type="checkbox" class="modal7__checkbox">
                                        <p class="modal7__price">
                                            90 000 руб. день

                                        </p>
                                    </div>
                                    <p class="modal7__banner-name">
                                        Перетяжка (баннер 1250х270) <br>
                                        На главной странице всех <br>пользователей
                                    </p>
                                </div>
                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>img/banner.png" alt="banner" class="modal7__banner-pic">
                    </div>
                    <div class="modal7__footer d-flex">
                        <p class="modal7__after">
                            В стоимость включена разработка баннера и его размещение. Стоимость указана без НДС.
                        </p>
                        <button class="modal7__btn">
                            Оплатить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window6" style="display: none">
        <div class="box-modal" id="wind6">
            <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close"></div>
            <div class="window6__container">
                <div class="modal6">
                    <h2 class="modal6__headline">
                        Обратная связь
                    </h2>
                    <div class="modal6__wrap d-flex">
                        <form action="#" class="modal6__form">
                            <p class="modal6__text">
                                Сообщите цель вашего обращения, мы обязательно свяжемся с Вами.
                            </p>
                            <textarea maxlength="150" class="modal6__textarea"></textarea>
                            <p class="modal6__text">
                                Ваш телефон:
                            </p>
                            <input type="tel" class="modal6__input">
                            <p class="modal6__text">
                                Ваш электронная почта:
                            </p>
                            <input type="email" class="modal6__input">
                        </form>
                        <div class="modal6__social-block">
                            <h2 class="modal6__title">
                                Присоединяйся к нам в социальной сети
                            </h2>
                            <div class="modal3__socials">
                                <div class="modal6__social-wrap">
                                    <a href="https://vk.com/" target="_blank" class="credits__web">
                                        <i class="fab fa-vk modal6__icon"></i>
                                    </a>
                                    <a href="https://www.facebook.com" target="_blank" class="credits__web">
                                        <i class="fab fa-facebook-f modal6__icon"></i>
                                    </a>
                                    <a href="https://twitter.com" target="_blank" class="credits__web">
                                        <i class="fab fa-twitter modal6__icon"></i>
                                    </a>
                                </div>
                                <div class="modal6__social-wrap2">
                                    <a href="https://www.youtube.com/" target="_blank" class="credits__web">
                                        <i class="fab fa-youtube modal6__icon"></i>
                                    </a>
                                    <a href="https://www.instagram.com" target="_blank" class="credits__web">
                                        <i class="fab fa-instagram modal6__icon"></i>
                                    </a>
                                </div>
                            </div>
                            <button class="modal6__btn">
                                Отправить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window5" style="display: none">
        <div class="box-modal" id="wind5">
            <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close"></div>
            <div class="modal5 modal5__indent_left">
                <div class="window5__container">
                    <h2 class="modal5__headline">
                        Утверждено
                    </h2>
                    <p class="modal5__position modal5__lh">
                        Индивидуальный предприниматель
                    </p>
                    <p class="modal5__name modal5__lh">
                        Горбунов Артемий Викторович <span class="modal5__ttu">Пользовательское соглашение</span>
                    </p>
                    <p class="modal__app modal5__lh">
                        об использовании мобильного приложения «B2Bmeet»
                    </p>
                    <p class="modal5__indent modal5__indent_left">
                        Настоящее Пользовательское соглашение (далее также Соглашение) является предложением
                        Индивидуального предпринимателя Горбунова Артемия Викторовича (ОГРНИП 317554300052514, ИНН
                        550201247998)(далее – Исполнитель) на заключение с любым лицом, совершившим акцепт настоящей
                        оферты (далее – Пользователь), Соглашения на условиях, изложенных ниже.
                    </p>
                    <p class="modal5__indent">
                        Акцепт оферты осуществляется действиями Пользователя, свидетельствующими об акцепте
                        (конклюдентные действия), в том числе, но не ограничиваясь, посредством установки мобильного
                        приложения и регистрации в нем. В случае акцепта настоящей оферты, между сторонами
                        заключается договор в электронной форме на основании п.2 ст. 434 и п. 3 ст. 438 Гражданского
                        кодекса РФ. Договор в электронной форме по юридической силе приравнивается к договору,
                        составленному в письменной форме и подписанному Сторонами, в том числе <br>удостоверенному
                        печатями Сторон.
                    </p>
                    <h2 class="title__intro modal5__lh ">
                        Условия Пользовательского соглашения:
                    </h2>
                    <p class="modal5__indent">
                        <p class="modal5__string">
                            1. Термины и определения
                        </p>
                        <p class="modal5__indent">
                            <b>1.1.Аналог собственноручной подписи</b> – комбинация логина и пароля, используемые
                            Пользователем для входа в мобильное приложение, и осуществления последующих действий в
                            мобильном приложении, в том числе оплата Услуг и осуществление иных конклюдентных
                            действий в мобильном приложении. <br>
                            <b>1.2.Логин</b> – номер мобильного телефона Пользователя. <br>
                            <b>1.3.Пароль</b> – комбинация цифр, которая предоставляется Пользователю Исполнителем
                            для входа в мобильное приложение путем направления ее на номер мобильного телефона
                            Пользователя в виде текстового сообщения. <br>
                            <b>1.4. Мобильное приложение</b> – предназначенная для установки и использования на
                            устройствепрограмма для ЭВМ, позволяющая Пользователю получить доступ к сервису с
                            использованием сети связи. Мобильным приложением является программа для ЭВМ «B2Bmeet»,
                            функционирующее на операционных системах Android, iOS, исключительное право на которое
                            принадлежит Исполнителю. Исполнитель предоставляет мобильное приложение Пользователю на
                            условиях простой (неисключительной) лицензии (подробнее раздел 8 Соглашения). <br>
                            <b>1.5.Устройство</b> – персональный компьютер, планшет, мобильный телефон,
                            коммуникатор, смартфон, иное устройство, позволяющее использовать мобильное приложение
                            по его функциональному назначению. <br>
                            <b>1.6. Сервис (Услуги)</b> – комплекс услуг, предоставляемый Исполнителем с помощью
                            мобильного приложения. Перечень Услуг и их стоимость могут быть изменены по усмотрению
                            Исполнителя без согласия Пользователя. Об изменении перечня услуг и их стоимости
                            Исполнитель уведомляет Пользователя по электронной почте или иным способом, в том числе
                            посредством размещения на Сайте или в мобильном приложении соответствующей информации.
                            <br>
                            <b>1.7. Стороны</b> – Исполнитель и Пользователь при их совместном упоминании, а
                            Сторона– любое из них при упоминании по отдельности. <br>
                            <b>1.8.Пользователь</b> - пользователь мобильного приложения, зарегистрированный в нем в
                            качестве Пользователя и использующий его в соответствии с его функциональным
                            назначением. Пользователем может быть полностью дееспособное физическое лицо, достигшее
                            восемнадцатилетнего возраста. <br>
                            <b>1.9.Администрация Сайта (Исполнитель)</b> - Индивидуальный предприниматель Горбунов
                            Артемий Викторович (ОГРНИП 317554300052514, ИНН 550201247998). <br>
                            <b>1.10.Сайт</b> – совокупность программных и аппаратных средств для ЭВМ, обеспечивающих
                            публикацию для обозрения информации и данных, объединенных общим целевым назначением,
                            посредством технических средств, применяемых для связи между ЭВМ в сети Интернет. Сайт
                            доступен по уникальному электронному адресу или его буквенному обозначению. Под Сайтом
                            понимается Сайт, расположенный в сети Интернет по адресу: www.b2bmeet.ru <br>
                            <b>1.11.Пользовательское соглашение</b> – настоящее соглашение, заключаемое между
                            Исполнителем и Пользователем. <br>
                            <b>1.12. Резервирование денежных средств</b> - перечисление Пользователем Исполнителю в
                            полном объеме Зарезервированной суммы. <br>
                            <b>1.13. Зарезервированная сумма</b> — денежная сумма, соответствующая стоимости
                            Встречи, в которую заранее включается комиссия Исполнителя, которая перечисляется
                            Пользователем Исполнителю для последующего осуществления выплаты Стоимости Работы (в том
                            числе соразмерно уменьшенной) Исполнителю в случаях, предусмотренных Договором. <br>
                            <b>1.14. Встреча</b> – результат использования мобильного приложения, при котором
                            Пользователи (п.1.15 и п.1.16) договариваются о проведение между собой переговоров.
                            Длительность встречи, оплата которой происходит в мобильном приложении, устанавливается
                            в размере 20 (двадцать) минут, по истечение которых Пользователи самостоятельно
                            продолжать встречу или нет. <br>
                            <b>1.15. Инициатор встречи</b> – Пользователь, отправивший предложение другому
                            Пользователю (ЛПР). <br>
                            <b>1.16. ЛПР</b> – лицо, принимающие приглашение на встречу от ее инициатора. <br>
                            <b>1.17. Профайл</b> – перечень данных, заполняемый Пользователем, а именно: <br>
                            ¾ Стоимость встречи <br>
                            ¾ Компания; <br>
                            ¾ ИНН; <br>
                            ¾ Сайт; <br>
                            ¾ Отрасль; <br>
                            ¾ Город; <br>
                            ¾ Теги; <br>
                            ¾ иные поля, предусмотренные в мобильном приложении. <br>
                            Исполнитель осуществляет проверку данных, указанных в Профайле, из открытых источников,
                            а также связывается с Пользователем для их проверки, однако не является гарантом
                            достоверности указанной информации. <br>
                            [E1] <br>
                            2.Предмет Соглашения <br>
                            2.1.По настоящему Соглашению Исполнитель обязуется за вознаграждение оказывать
                            Пользователю Услуги, представленные в мобильном приложении, а именно: <br>
                            ¾ регистрация Пользователя на в мобильном приложении; <br>
                            ¾ предоставление функционала мобильного приложения, направленного на поиск и организацию
                            встреч с другими Пользователями; <br>
                            ¾ предоставление консультаций и технической поддержки по работе мобильного приложения;
                            <br>
                            ¾ оказания иных Услуг, представленных в мобильном приложении. <br>
                            2.2. Исполнитель вправе привлекать иных лиц для оказания Услуг, оставаясь при этом
                            ответственным за их оказание перед Пользователем. <br>
                            2.3. Настоящий Договор имеет смешанную правовую природу: <br>
                            2.3.1. Отношения между Пользователем и Исполнителем строятся по модели договора
                            возмездного оказания услуг (глава 39 Гражданского кодекса Российской Федерации) в части
                            предоставления всех возможностей мобильного приложения, за исключением оказания
                            посреднических Услуг по резервированию денежных средств. <br>
                            2.3.2. Отношения между Пользователем и Исполнителем строятся по модели агентского
                            договора (глава 52 Гражданского кодекса Российской Федерации) в части предоставления
                            Услуг по резервированию денежных средств и их последующей выплате Пользователю (ЛПР).
                            <br>
                            3.Взаимодействие Сторон и порядок оказания Услуг <br>
                            3.1.Стороны пришли к соглашению, что их взаимодействие посредством электронной переписки
                            с использованием адресов электронных почт и (или) посредством осуществления
                            конклюдентных действий в мобильном приложении, т.е. действий, которые свидетельствуют о
                            воле и намерении на совершение определенных действий, является официальным и имеет
                            юридическую силу, и направление любой информации, уведомлений и документов может
                            осуществляться в форме описанного выше взаимодействия. <br>
                            Такое направление любой информации, уведомлений и документов сохраняется при помощи
                            программно-технических средств мобильного приложения.Стороны подтверждают, что лица,
                            осуществляющие коммуникацию с электронных адресов или посредством осуществления
                            конклюдентных действий на в мобильном приложении, являются уполномоченными с каждой из
                            Сторон в полном объеме для совершения любых действий в рамках настоящего Соглашения, а
                            также для подписания любых документов, связанных с исполнением настоящего Соглашения,
                            без предоставления дополнительных доверенностей. Стороны гарантируют, что любые
                            документы, предоставляемые в виде электронном виде, в том числе, но не ограничиваясь, в
                            виде сканированных копий или факсов, имеют юридическую силу. <br>
                            3.2. После регистрации в мобильном приложении Пользователь может пользоваться всеми его
                            возможностями. Использования мобильного приложения, Исполнитель предоставляет
                            возможность заполнения Профайла.
                            3.3. Инициатор встречи направляет приглашение ЛПР, которые его заинтересовали и с
                            которым последний хотел бы встретиться. <br>
                            3.4. После получения приглашения на встречу ЛПР вправе принять ее либо отказать
                            инициатору встречи. В случае согласия на проведение встречи, инициатор получает
                            уведомление об этом. <br>
                            3.5. Инициатор должен произвести оплату встречи после получения уведомления о принятии
                            приглашения на встречу ЛПР. Оплата производится с помощью программно-технических средств
                            мобильного приложения. Инициатор при оплате услуг видит стоимость встречи, в которую уже
                            заранее включена комиссия Исполнителя. Оплата встречи резервируется Исполнителем для
                            последующей выплаты ЛПР. <br>
                            3.6. После оплаты встречи, Исполнитель посредством мобильного приложения предоставляет
                            инициатору встречи номер мобильного телефона ЛПР. Дальнейшие действия по обсуждению
                            места, времени и иных условий встречи, инициатор и ЛПР проводят самостоятельно.
                            Исполнитель не отвечает за результат встречи, а также любые ожидания Пользователей от
                            проведения встречи. Оказание услуг Исполнителем по организации встречи считается
                            оконченным после предоставления инициатору встречи номера контактного телефона ЛПР. <br>
                            3.7. По итогам встречи, Исполнитель перечисляет зарезервированную сумму ЛПР. Для этого,
                            Инициатор встречи должен произвести соответствующие действия в мобильном приложении –
                            принять результат встречи.
                            Если инициатор встречи не осуществляет принятие результата встречи в течение 2 (двух)
                            рабочих дней с момента резервирования денежных средств, то Исполнитель связывается с ним
                            для уточнения причин, а также связывается с ЛПР. <br>
                            В случае, если Исполнитель посчитает, что ЛПР выполнил свои обязательства по встрече, а
                            также что инициатор встречи не был введен ЛПР в заблуждения относительно его должности,
                            места работы и иных параметров, то Исполнитель перечисляет зарезервированную сумму ЛПР
                            по своему усмотрению.
                            В случае, если Исполнитель придет к выводу, что обязательства ЛПР не были исполнены и
                            (или) последний представил о себе недостоверные сведения относительно его должности,
                            места работы и иных параметров, то Исполнитель вправе отказать ЛПР в перечислении
                            зарезервированной суммы и перечислить ее инициатору за вычетом расходов платежной
                            системы и (или) банковской комиссии на это. <br>
                            <span class="modal5__empty-string">[E2]</span> <br>
                            Инициатор встречи и ЛПР заранее соглашаются с любым решением Исполнителя. <br>
                            3.8. Инициатор встречи также вправе поставить оценку проведенной встрече. Исполнитель
                            вправе связаться с ним для получения обратной связи о проведенной встрече. <br>
                            4.Вознаграждение (комиссия) Исполнителя и расчеты Сторон <br>
                            4.1.Вознаграждение Исполнителя (стоимость Услуг) составляет 20% (двадцать процентов) от
                            стоимости встречи с ЛПР, указанной в его Профайле, которая начисляется сверх стоимости
                            встречи ЛПР. <br>
                            4.2. Расчеты Сторон происходят в безналичной форме с помощью использования
                            программно-технических средств мобильного приложения. Все расчеты по настоящему
                            Соглашению осуществляются исключительно в валюте Российской Федерации – в рублях. <br>
                            5.Ответственность сторон. Отказ от гарантий <br>
                            5.1.За неисполнение или ненадлежащее исполнение обязательств по настоящему Соглашению
                            Стороны несут ответственность в соответствии с действующим законодательством Российской
                            Федерации. <br>
                            5.2.Стороны освобождаются от ответственности за неисполнение или ненадлежащее исполнение
                            обязательств по Соглашению при возникновении непреодолимой силы, то есть чрезвычайных и
                            непредотвратимых при данных условиях обстоятельств, под которыми понимаются запретные
                            действия властей, гражданские волнения, эпидемии, блокада, эмбарго, землетрясения,
                            наводнения, пожары или другие стихийные бедствия. <br>
                            5.3. Все споры и разногласия, возникающие между Сторонами в процессе исполнения
                            настоящего Соглашения, решаются путем переговоров. <br>
                            5.4. Все споры и разногласия, возникающие из настоящего Соглашения или в связи с ним, в
                            том числе касающиеся его выполнения, нарушения, прекращения или действительности, если
                            они не урегулированы путем двусторонних переговоров, подлежат разрешению суде по месту
                            нахождения Исполнителя. <br>
                            5.5. До обращения в соответствующий суд заинтересованная сторона направляет другой
                            стороне претензию, срок ответа на которую устанавливается 20 (двадцать) календарных дней
                            с момента её получения, в том числе с момента получения по электронной почте или в
                            сообщении на непосредственно в мобильном приложении. <br>
                            5.6. Мобильное приложение предоставляется по принципу «как есть», то есть без гарантий
                            качества и пригодности для каких-либо явных или подразумеваемых целей. <br>
                            Исполнитель не гарантирует, что мобильное приложение будет работать беспрерывно и
                            безошибочно. Исполнитель вправе без объяснения причин отказать в доступе к мобильному
                            приложению всем Пользователям или любому из них на время или навсегда, удалить любую
                            информацию или любой контент, размещенные пользователем в мобильном приложении.
                            Исполнитель оставляет за собой право в любое время приостановить предоставление услуг
                            Пользователям без объяснения причин. Исполнитель не гарантирует, что размещаемая им
                            информация не может причинить пользователю моральный вред, вред здоровью либо убытки.
                            <br>
                            <span class="modal5__empty-string">[E3]</span> <br>
                            5.7. Пользователь соглашается с тем, что любая передача любой информации по сети
                            Интернет, в том числе по защищённым каналам связи в зашифрованном виде, не может быть
                            гарантированно защищена от несанкционированного доступа к ней третьими лицами. В связи с
                            этим Исполнитель не несет ответственности за любой ущерб, причиненный Пользователю
                            вследствие несанкционированного доступа третьих лиц к информационным материалам
                            Пользователя. <br>
                            5.8. Ни одна из Сторон не несет никакой ответственности перед другой Стороной за
                            упущенную выгоду, иные косвенные убытки вне зависимости от того, могла ли такая Сторона
                            предвидеть возможность причинения таких убытков другой Стороне в конкретной ситуации или
                            нет. <br>
                            <b>6. Заключение, вступление в силу, изменение Пользовательского соглашения</b> <br>
                            6.1. Соглашение считается заключенным и вступает в силу в момент его акцепта с помощью
                            программно-технических средств мобильного приложения, в том числе в случае оплаты Услуг
                            в мобильном приложении. <br>
                            6.2. В целях подтверждения наличия заключенного между Сторонами Соглашения, Стороны по
                            согласованию могут подписать между собой настоящее Соглашение в письменной форме. В
                            таком случае Соглашение должно быть подписано уполномоченными представителями Сторон в
                            количестве двух идентичных экземпляров, по одному для каждой из Сторон. <br>
                            6.3. Исполнитель вправе вносить изменения в настоящее Соглашение без согласия
                            Пользователя. Информация об изменении размещается на в мобильном приложении и (или) на
                            Сайте. <br>
                            6.4. Новая редакция Соглашения вступает в силу с момента ее опубликования в мобильном
                            приложении, если иное не предусмотрено новой редакцией Соглашения. <br>
                            6.5. Все предложения или вопросы по настоящему Соглашению следует направлять по адресу
                            электронной почты: указать адрес. <br>
                            7.Конфиденциальность <br>
                            7.1.Стороны, получившие в целях исполнения своих обязательств по настоящему Соглашению
                            конфиденциальную информацию, сведения, составляющие коммерческую тайну, не вправе
                            сообщать эти сведения третьим лицам без письменного разрешения другой Стороны настоящего
                            Соглашения, за исключением случаев, установленных законом. <br>
                            7.2.При нарушении обязанности, предусмотренной в п. 7.1 настоящего Соглашения, Стороны
                            несут ответственность в соответствии с нормами законодательства Российской Федерации.
                            <br>
                            7.3. Пользователь соглашается с условиями, целями и порядком обработки персональных
                            данных на условиях <b>Политики в области обработки персональных данных</b>, размещенной
                            на Сайте по адресу: адрес Политики, а также дает свое <b>Согласие на обработку
                                персональных данных</b>, текст которого размещен по адресу: ________ <br>
                            8.Интеллектуальная собственность <br>
                            8.1. Исполнитель обязуется предоставляет Пользователю право использования мобильного
                            приложения (простую (неисключительную) лицензию), исключительное право на которое
                            принадлежит Исполнителю. <br>
                            8.2. В объем права использования мобильного приложения, предоставленного Пользователю,
                            входит использование мобильного приложения по его прямому функциональному назначению,
                            том числе установка и воспроизведение мобильного приложения на неограниченном числе
                            устройств, при условии сохранения в неизменном виде комбинации, состава и содержания
                            мобильного приложения по сравнению с тем, как они предоставляются для использования
                            Исполнителем. <br>
                            8.3. Территория действия настоящего Соглашения ограничена Российской Федерацией. <br>
                            8.4. Срок действия простой (неисключительной) лицензии на использование мобильного
                            приложения, равен сроку действия исключительного права на мобильное приложение. <br>
                            При расторжении или прекращении действия Соглашения Пользователь утрачивает право
                            использования мобильного приложения. Исполнитель вправе в любой момент без объяснения
                            причин расторгнуть настоящее Соглашение, прекратив использование мобильного приложения
                            Пользователем. <br>
                            8.5. Если в тексте Соглашения специально не указано иное, то Пользователь не может без
                            предварительного письменного согласия Исполнителя: <br>
                            ¾ модифицировать, встраивать мобильное приложение в другое программное обеспечение или
                            объединять с ним, создавать переработанную версию любой части мобильного приложения;
                            <br>
                            ¾ продавать, выдавать лицензии (сублицензии), отдавать в аренду, переуступать,
                            передавать, отдавать в залог, разделять права по настоящему Соглашению третьим лицам;
                            <br>
                            ¾ использовать, копировать, распространять или воспроизводить мобильное приложение в
                            интересах третьих лиц, а также в коммерческих целях; <br>
                            ¾ обнародовать результаты какого-либо сопоставительного анализа касательно мобильного
                            приложения, использовать упомянутые результаты для какой-либо деятельности по разработке
                            программного обеспечения;
                            ¾ модифицировать, дизассемблировать, декомпилировать, разбирать на составляющие коды,
                            перерабатывать или усовершенствовать мобильное приложение, пытаться получить исходный
                            текст программы мобильного приложения, иным способом нарушать нормальный ход его работы.
                            <br>
                            ¾ копировать, воспроизводить, перерабатывать, распространять, размещать в свободном
                            доступе (опубликование) в сети Интернет, использовать в средствах массовой информации и
                            (или) коммерческих целях любые материалы, размещенные в мобильном приложении, в том
                            числе как извлеченные из баз данных, включаемых в состав мобильного приложения, так и
                            полученных путем копирования результатов обработки данных с использованием мобильного
                            приложения, а также производных от таких материалов продуктов (с дополнениями,
                            сокращениями и прочими переработками). <br>
                            Права и способы использования Мобильного приложения, в явном виде не предоставленные (не
                            разрешенные) Пользователю по Соглашению, считаются не предоставленными (запрещенными)
                            Исполнителем. <br>
                            9.Заключительные положения <br>
                            9.1.Заключая настоящее Соглашение, Пользователь гарантирует, что он обладает полной
                            правоспособностью и дееспособностью, и имеет право заключать настоящее Соглашение. <br>
                            9.2. В случае возникновения разногласий между Сторонами сведения, которые зафиксированы
                            при помощи технических средств мобильного приложения, имеют преимущественное значение
                            при разрешении таких разногласий.<br>
                            <span class="modal5__empty-string">[E4]</span>
                            9.3.Стороны принимают во внимание и соглашаются, что исполнение, неисполнение или
                            ненадлежащее исполнение обязательств по Соглашению может влиять на статус Пользователя,
                            в соответствии с правилами мобильного приложения, сведения об этом могут отражаться на
                            соответствующих страницах мобильного приложения, использоваться иным образом в связи с
                            функционированием мобильного приложения и Сайта. <br>
                            9.4.Пользователь гарантирует, что пользуются мобильным приложением в соответствии с
                            условиями и правилами его использования, информация о нем в Профайле отражена правильным
                            и полным образом, он действует под собственным, а не вымышленным именем, все действия,
                            совершенные на в мобильном приложении, совершаются ими лично или уполномоченными лицами,
                            обязательны и юридически действительны для Пользователя. Стороны обязуются сообщать друг
                            другу о любом факте несанкционированного разглашения информации третьим лицам. В случае
                            завладения третьим лицом логином и паролем Пользователя и совершения им от имени
                            Пользователя действий в мобильном приложении, которые привели или могут привести к
                            причинению ему убытков, Исполнитель не несет ответственности. <br>
                            9.5.Все изменения к Соглашению, переписка между Сторонами, уведомления и обращения
                            осуществляются исключительно на русском языке. <br>
                            9.6. Действующее Соглашение размещено на странице Сайта по адресу: ссылка, а также в
                            мобильном приложении: <br>
                            9.7. Неотъемлемыми частями настоящего Соглашения, которые Пользователь безоговорочно
                            принимает (акцептует) при заключении Соглашения являются: <br>
                            - Согласие на обработку персональных данных, размещенное на Сайте по адресу: ссылка.
                            <br>
                            - Политика в области обработки персональных данных, размещенное на сайте: ссылка. <br>
                            9. Реквизиты Исполнителя <br>
                            <b>Администрация сайта (Исполнитель):</b> <br>
                            Индивидуальный предприниматель Горбунов Артемий Викторович (ОГРНИП 317554300052514, ИНН
                            550201247998)
                            Местонахождение: указать адрес места вашей деятельности <br>
                            <span class="modal5__empty-string">[E1]</span>
                            Вы писали, что проверка идет с разных сторон – надо уточнить как именно, по возможности.
                            <br>
                            [E2] <br>
                            Вы так конкретно и не ответили мне вчера на этот вопрос <br>
                            Я решил по своему усмотрению это описать. <br>
                            Такие правила нужны в любом случае, иначе будет непонятно как работает мобильное
                            приложение и к нему не будет доверия от Пользователей. <br>
                            [E3] <br>
                            Это стандартная формулировка для онлайн сервисов, но может и подкорректировать, если
                            захотите <br>
                            [E4] <br>
                            <span class="modal5__last-string">
                                Думаю, что у вас такое взаимодействие будет фиксироваться, поэтому будет удобно
                            </span>
                        </p>
                </div>
            </div>
        </div>
    </div>

    <div id="window4" style="display: none">
        <div class="window4__container">
            <div class="box-modal" id="wind4">
                <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close">
                </div>
                <div class="modal4 d-flex">
                    <div class="modal4__content">
                        <p class="modal4__title">
                            Для связи инвесторов с нами
                        </p>
                        <h2 class="modal4__headline">
                            О B2Bmeet
                        </h2>
                        <p class="modal4__text">
                            Мы создали и развиваем онлайн бизнес платформу в России и СНГ делаем упор на облегчение
                            процесса организации бизнес встреч, передачи коммерческих предложений для миллионов
                            руководителей и собственников бизнеса в России. Наша база данных содержит более 4,2 млн
                            данных представителей бизнеса и наша платформа обеспечивает в среднем организацию более
                            500 встреч в день!
                        </p>
                        <p class="modal4__undertext">
                            Зарегистрируйтесь и подпишитесь на уведомления для инвесторов по электронной почте:
                        </p>
                        <form action="#" class="modal4__form">
                            <div class="modal4__intro d-flex">
                                <div class="modal4__wrap-form">
                                    <p class="modal4__intro-text">
                                        Ваше имя
                                    </p>
                                    <input type="text" class="modal4__input">
                                </div>
                                <div class="modal4__wrap-form">
                                    <p class="modal4__intro-text">
                                        Ваш телефон
                                    </p>
                                    <input type="text" class="modal4__input">
                                </div>
                            </div>
                            <div class="modal4__intro d-flex">
                                <div class="modal4__wrap-form">
                                    <p class="modal4__intro-text">
                                        Ваш адрес электронной почты
                                    </p>
                                    <input type="text" class="modal4__input">
                                </div>
                                <div class="modal4__wrap-form">
                                    <div class="modal4__extra-wrap modal4__extra-wrap_first d-flex">
                                        <input type="checkbox" class="modal4__checkbox">
                                        <p class="modal4__checkbox-text">
                                            Мероприятия B2Bmeet
                                        </p>
                                    </div>
                                    <div class="modal4__extra-wrap modal4__extra-wrap_second d-flex">
                                        <input type="checkbox" class="modal4__checkbox">
                                        <p class="modal4__checkbox-text">
                                            Новости B2Bmeet
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal4__credits">
                        <img src="<?php echo base_url(); ?>img/man__square.jpg" alt="man" class="modal4__man">
                        <p class="modal4__founder">
                            Горбунов Артемий
                        </p>
                        <p class="modal4__description">
                            основатель проекта
                        </p>
                        <button type="submit" class="modal4__btn">
                            Подписаться
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window3" style="display: none">
        <div class="window3__container">
            <div class="box-modal" id="wind3">
                <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close">
                </div>
                <div class="modal3 d-flex">
                    <div class="modal3__wrap">
                        <div class="modal3__socials">
                            <div class="modal3__social-wrap">
                                <a href="https://vk.com/" target="_blank" class="credits__web">
                                    <i class="fab fa-vk modal3__icon"></i>
                                </a>
                                <a href="https://www.facebook.com" target="_blank" class="credits__web">
                                    <i class="fab fa-facebook-f modal3__icon"></i>
                                </a>
                                <a href="https://twitter.com" target="_blank" class="credits__web">
                                    <i class="fab fa-twitter modal3__icon"></i>
                                </a>
                            </div>
                            <div class="modal3__social-wrap2">
                                <a href="https://www.youtube.com/" target="_blank" class="credits__web">
                                    <i class="fab fa-youtube modal3__icon"></i>
                                </a>
                                <a href="https://www.instagram.com" target="_blank" class="credits__web">
                                    <i class="fab fa-instagram modal3__icon"></i>
                                </a>
                            </div>
                        </div>
                        <h2 class="modal3__follow">
                            Присоединяйся к нам в социальной сети
                        </h2>
                    </div>
                    <div class="modal3__vacancies">
                        <h3 class="modal3__title">
                            Наши вакансии
                        </h3>
                        <p class="modal3__text">
                            В настоящее время у нас нет открытых вакансий, но мы можем сделать исключение, поэтому
                            просим вас направить нам свое резюме с указание вашего опыта, навыков и ожидаемого
                            уровня оплаты труда.Как вы думаете чем мы будем полезны друг другу?
                        </p>
                        <form action="#" class="modal3__form">
                            <textarea name="#" maxlength="400" class="modal3__textarea"></textarea>
                            <div class="modal3__cv-wrap d-flex">
                                <a href="#" class="modal3__attach">
                                    Прикрепить резюме
                                </a>
                                <a href="#" class="modal3__link">
                                    <i class="far fa-id-card"></i>
                                </a>
                                <a href="#" class="modal3__link">
                                    <i class="fas fa-window-close"></i>
                                </a>
                            </div>
                            <p class="modal3__info">
                                Получить информацию о новых вакансиях
                            </p>
                            <div class="modal3__send d-flex">
                                <span class="modal3__mail-text">
                                    Email
                                </span>
                                <input type="text" class="modal3__input">
                                <button class="modal3__btn">
                                    Отправить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window2" style="display: none;">
        <div class="window2__container">
            <div class="box-modal" id="wind2">
                <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close">
                </div>
                <div class="modal2 d-flex">
                    <img src="<?php echo base_url(); ?>img/sertificate.png" alt="sertificate" class="modal2__sertificate">
                    <div class="modal2__content">
                        <p class="modal2__text">
                            В соответствии с законодательством Российской Федерации незаконное использование
                            товарного знака влечет за собой гражданскую, административную и уголовную
                            ответственность.
                        </p>
                        <p class="modal2__text">
                            По вопросам заключение лицензионного соглашение на использование товарного знака
                            (ст.1489 ГК РФ) просим оставить ваши контактные данные.
                        </p>
                        <p class="modal2__text modal2__text_last">
                            Ваш контактный телефон
                        </p>
                        <form action="#" class="modal2__form">
                            <input type="tel" class="modal2__input">
                        </form>
                        <button type="submit" class="modal2__btn">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window12" style="display: none">
        <div class="box-modal" id="wind12">
            <div class="box-modal_close arcticmodal-close">
                <img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close" />
            </div>
            <div class="window12__container">
                <div class="modal12 d-flex">
                    <div class="modal12__block">
                        <div class="modal12__wrap d-flex">
                            <img src="<?php echo base_url(); ?>img/apple-store.jpg" alt="apple-store" class="modal12__apple" />
                            <img src="<?php echo base_url(); ?>img/google-play.jpg" alt="google-play" class="modal12__google" />
                        </div>
                        <div class="modal12__social">
                            <div class="modal12__social-wrap">
                                <a href="https://vk.com/" target="_blank" class="modal12__web">
                                    <i class="fab fa-vk credits__icon"></i>
                                </a>
                                <a href="https://www.facebook.com" target="_blank" class="modal12__web">
                                    <i class="fab fa-facebook-f credits__icon"></i>
                                </a>
                                <a href="https://twitter.com" target="_blank" class="modal12__web">
                                    <i class="fab fa-twitter credits__icon"></i>
                                </a>
                            </div>
                            <div class="modal12__social-wrap2">
                                <a href="https://www.youtube.com/" target="_blank" class="modal12__web">
                                    <i class="fab fa-youtube credits__icon"></i>
                                </a>
                                <a href="https://www.instagram.com" target="_blank" class="modal12__web">
                                    <i class="fab fa-instagram credits__icon"></i>
                                </a>
                            </div>
                        </div>
                        <p class="modal12__network">
                            Присоединяйся к нам в социальной сети
                        </p>
                    </div>
                    <iframe src="https://www.youtube.com/embed/yGafeJQoQ7s" class="modal12__video"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div id="window11" style="display: none">
        -
        <div class="box-modal" id="wind11">
            <div class="box-modal_close arcticmodal-close">
                <img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close" />
            </div>
            <div class="window10__container">
                <div class="modal11">
                    <div class="modal11__wrap d-flex">
                        <div class="modal11__content">
                            <p class="modal11__text">
                                Мы уведомим специалистов интересующей вас
                                отрасли, о важном вашем событии, направим
                                ваше письмо и смс, быстро и качественно
                            </p>
                            <div class="modal11__block">
                                <div class="modal11__item">
                                    <input type="checkbox" class="modal11__checkbox" />
                                    <span class="modal11__method">
                                        СМС
                                    </span>
                                </div>
                                <p class="modal11__text">
                                    Стоимость отправки одного СМС составляет
                                    5 рублей, без НДС. 1 сообщение не более
                                    70 знаков, включая пробелы
                                </p>
                            </div>
                            <div class="modal11__block">
                                <div class="modal11__item">
                                    <input type="checkbox" class="modal11__checkbox" />
                                    <span class="modal11__method">
                                        E-mail
                                    </span>
                                </div>
                                <p class="modal11__text">
                                    Стоимость отправки одного email
                                    составляет 20 рублей, без НДС.
                                </p>
                            </div>
                            <p class="modal11__text modal11__text_price">
                                Стоимость подготовки шаблона письма HTML -
                                350 $
                            </p>
                        </div>
                        <form action="#" class="modal11__form">
                            <div class="modal11__bracket d-flex">
                                <span class="modal11__title">
                                    ФИО
                                </span>
                                <input type="text" class="modal11__input modal11__input_name" />
                            </div>
                            <div class="modal11__bracket d-flex">
                                <span class="modal11__title">
                                    Телефон
                                </span>
                                <input type="tell" class="modal11__input modal11__input_tel" />
                            </div>
                            <div class="modal11__bracket d-flex">
                                <span class="modal11__title">
                                    Ваш email
                                </span>
                                <input type="email" class="modal11__input modal11__input_mail" />
                            </div>
                            <p class="modal11__title">
                                Сообщите текст и тему вашего сообщения
                            </p>
                            <textarea class="modal11__textarea"></textarea>
                            <div class="modal11__intro">
                                <div class="modal11__cover d-flex">
                                    <div class="modal11__intro d-flex">
                                        <input type="checkbox" class="modal11__checkbox" />
                                        <p class="modall11__text">
                                            Минимальная сумма заказа услуги
                                            15 000 рублей.
                                        </p>
                                    </div>
                                    <div class="modal11__btn">
                                        Заказать
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal11__footer">
                        <p class="modal11__text">
                            B2Bmeet использует для отправки писем
                            <a href="https://www.unisender.com" class="modal11__link">UniSender</a>, cервис
                            соответствует закону № 152-ФЗ «О
                            персональных данных» и внесен в реестр
                            операторов Роскомнадзора.
                        </p>
                        <p class="modal11__text modal11__text_last">
                            Вся информация размещена на серверах в
                            сертифицированных датацентрах класса
                            безопасности TIER3.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window10" style="display: none">
        <div class="box-modal" id="wind10">
            <div class="box-modal_close arcticmodal-close">
                <img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close" />
            </div>
            <div class="window10__container">
                <div class="modal10 d-flex">
                    <div class="modal10__links d-flex">
                        <a href="#" class="modal10__link">
                            Отправь приглашения по отрасли из ТОП-50
                        </a>
                        <a href="#" class="modal10__link">
                            Ваш баннер внутри приложения и на сайте
                        </a>
                        <a href="#" class="modal10__link">
                            Подружись с нами
                        </a>
                        <div class="credit__social d-flex">
                            <div class="credit__social-wrap">
                                <a href="https://vk.com/" target="_blank" class="credits__web">
                                    <i class="fab fa-vk credits__icon"></i>
                                </a>
                                <a href="https://www.facebook.com" target="_blank" class="credits__web">
                                    <i class="fab fa-facebook-f credits__icon"></i>
                                </a>
                                <a href="https://twitter.com" target="_blank" class="credits__web">
                                    <i class="fab fa-twitter credits__icon"></i>
                                </a>
                            </div>
                            <div class="credit__social-wrap2">
                                <a href="https://www.youtube.com/" target="_blank" class="credits__web">
                                    <i class="fab fa-youtube credits__icon"></i>
                                </a>
                                <a href="https://www.instagram.com" target="_blank" class="credits__web">
                                    <i class="fab fa-instagram credits__icon"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="modal10__content">
                        <div class="modal10__text">
                            Мы готовы взять на себя организацию встречи с
                            представителем интересующей вас компании,
                            стоимость услуг составляет 40 000 рублей, без
                            НДС. Срок исполнения до 5 рабочих дней.
                            Приглашенному участнику за встречу вы
                            оплачиваете отдельно, через сервис b2bmeet
                        </div>
                        <p class="modal10__action">
                            Мы вернем вам деньги если не договоримся о
                            встрече!
                        </p>
                        <form action="#" class="modal10__form">
                            <p class="modal10__description">
                                ФИО с кем планируете встретиться
                            </p>
                            <input type="text" class="modal10__input" />
                            <p class="modal10__description">
                                Компания, место работы
                            </p>
                            <input type="text" class="modal10__input" />
                            <p class="modal10__description">
                                Цель встречи
                            </p>
                            <textarea class="modal10__textarea"></textarea>
                            <button class="modal10__btn">
                                Оплатить
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window9" style="display: none">
        <div class="box-modal" id="wind9">
            <div class="box-modal_close arcticmodal-close">
                <img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close" />
            </div>
            <div class="window9__container">
                <div class="modal9">
                    <h2 class="modal9__headline">
                        визитка B2Bmeet
                    </h2>
                    <p class="modal9__text">
                        Привлекайте клиентов, разместив свою визитную
                        карточку в социальных сетях, в блоге или на сайте, и
                        получайте деньги за встречу с Вами. Создайте свою
                        визитную карточку:
                    </p>
                    <div class="modal9__wrap d-flex">
                        <div class="modal9__card-square">
                            <div class="modal9__detail d-flex">
                                <input type="checkbox" class="modal9__input" />
                                <span class="modal9__word">
                                    Квадратная
                                </span>
                            </div>
                            <img src="<?php echo base_url(); ?>img/square-coins.png" alt="coin" class="modal9__square-pic" />
                        </div>
                        <div class="modal9__card-round">
                            <div class="modal9__detail d-flex">
                                <input type="checkbox" class="modal9__input" />
                                <span class="modal9__word">
                                    Круглая
                                </span>
                            </div>
                            <img src="<?php echo base_url(); ?>img/red-back.png" alt="coin" class="modal9__round-pic" />
                        </div>
                    </div>
                    <textarea class="modal9__textarea">
                        Пример промо-кода
                    </textarea>
                    <a href="#" class="modal9__link">
                        Скопировать код
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="window8" style="display: none">
        <div class="box-modal" id="wind8">
            <div class="box-modal_close arcticmodal-close">
                <img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close" />
            </div>
            <div class="window8__container">
                <div class="modal8 d-flex">
                    <div class="modal8__links d-flex">
                        <a href="#" class="modal8__link">
                            Доверь организацию встречи B2Bmeet
                        </a>
                        <a href="#" class="modal8__link">
                            Отправь приглашения по отрасли из ТОП-50
                        </a>
                        <a href="#" class="modal8__link">
                            Ваш баннер внутри приложения и на сайте
                        </a>
                        <a href="#" class="modal8__link">
                            Подружись с нами в инстаграмм
                        </a>
                    </div>
                    <div class="modal8__text">
                        Приложение обеспечивает анонимность, участников.
                        Оплата за встречу проходит заранее, через приложение
                        B2B meet, что исключает наличные взаиморасчеты
                        Приглашающий на встречу оплачивает - организацию
                        встречи, данная оплата не подразумевает обязанностей
                        приглашенного, заключить контракт в пользу
                        инициатора. Приложение не помогает в организации
                        встреч с представителями органов государственной
                        власти, а работает только с руководителями
                        коммерческих компаний.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="window1" style="display: none;">
        <div class="window1__container">
            <div class="box-modal" id="wind1">
                <div class="box-modal_close arcticmodal-close"><img src="<?php echo base_url(); ?>img/close.png" alt="" class="reg__close">
                </div>
                <div class="reg" id="reg">
                    <div class="reg-wrap d-flex">
                        <div class="reg__card">
                            <p class="reg__text">
                                Зарегистрируйтесь сейчас и получите подарок!
                            </p>
                            <h2 class="reg__headline">
                                Получите свою визитную карточку для социальных сетей <span class="reg__capital">
                                    бесплатно!
                                </span>
                            </h2>
                            <p class="reg__text reg__text_last">
                                Привлекайте клиентов, разместив свою визитную карточку в социальных сетях, в блоге
                                или на сайте, и получайте деньги за встречу с Вами
                            </p>
                        </div>
                        <div class="reg__data">
                            <div class="reg__tabs d-flex">
                                <span class="reg__item">
                                    Немного о Вас
                                </span>
                                <span class="reg__item">
                                    О работе
                                </span>
                            </div>
                            <div class="reg__container">
                                <form action="#" class="reg__form" id="#tab1">
                                    <div class="reg__intro d-flex">
                                        <div class="reg__cover">
                                            <p class="reg__name">
                                                Имя
                                            </p>
                                            <input type="text" class="reg__name-enter">
                                        </div>
                                        <div class="reg__cover">
                                            <p class="reg__surname">
                                                Фамилия
                                            </p>
                                            <input type="text" class="reg__surname-enter">
                                        </div>
                                    </div>
                                    <p class="reg__position">
                                        Должность
                                    </p>
                                    <input type="text" class="reg__position-enter">
                                    <p class="reg__email">
                                        Личная email
                                    </p>
                                    <input type="email" class="reg__email-enter">
                                    <p class="reg__mobile">
                                        Мобильный телефон
                                    </p>
                                    <input type="tel" class="reg__mobile-enter">
                                </form>
                                <form action="#" class="reg__form reg__form_two" id="#tab2">
                                    <div class="reg__intro reg__intro_second d-flex">
                                        <div class="reg__cover reg__cover_company">
                                            <p class="reg__company">
                                                Наименование компании
                                            </p>
                                            <input type="text" class="reg__company-enter">
                                        </div>
                                        <div class="reg__cover reg__cover_city">
                                            <p class="reg__city">
                                                Город
                                            </p>
                                            <input type="text" class="reg__city-enter">
                                        </div>
                                    </div>
                                    <div class="reg__intro reg__intro_second d-flex">
                                        <div class="reg__cover">
                                            <p class="reg__tax">
                                                ИНН
                                            </p>
                                            <input type="number" class="reg__tax-enter">
                                        </div>
                                        <div class="reg__cover reg__cover_site">
                                            <p class="reg__site">
                                                Сайт
                                            </p>
                                            <input type="url" class="reg__site-enter">
                                        </div>
                                    </div>
                                    <div class="reg__cover_branch">
                                        <p class="reg__branch">
                                            Отрасль компании
                                        </p>
                                        <input type="text" class="reg__branch-enter">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="reg-footer">
                        <div class="reg-footer__wrap d-flex">
                            <img src="<?php echo base_url(); ?>img/red-back.png" alt="coins" class="reg-footer__img">
                            <div class="reg-footer__intro">
                                <h2 class="reg-footer__headline">
                                    Сколько хотите заработать за встречу?
                                </h2>
                                <div class="reg-wrap d-flex">
                                    <form action="#" class="reg-footer_form">
                                        <input type="text" class="reg-footer__input">
                                    </form>
                                    <div class="reg-wrap">
                                        <p class="reg-footer__text">
                                            Оплата встречитчерез B2Bmeet не обязывает Вас соглашаться на
                                            предложенитя которые Вам предлагают на встрече
                                        </p>
                                        <div class="reg-footer__wrap d-flex">
                                            <input type="checkbox" class="reg-footer__checkbox">
                                            <p class="reg-footer__agree">
                                                Вы согласны с политикой
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="reg-footer__arrow">
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>