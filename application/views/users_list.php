<?php foreach($users as $user){ ?>
	<div class="center__block center__block_link d-flex" data-user-id="<?=$user->id;?>">
        <div class="center__text">
            <h2 class="center__name">
                <?=$user->first_name;?>
            </h2>
            <h3 class="center__position">
                <?=$user->post;?>
            </h3>
            <div class="center__company d-flex">
                <div class="center__title">
                    <?=$user->company;?>
                </div>
                <a href="tel:77056756754" class="center__phone">
                    <?=$user->inn;?>
                </a>
            </div>
            <div class="center__credits d-flex">
                <h4 class="center__city">
                    <?=$user->city;?>
                </h4>
                <a href="www.nbfr.ru" class="center__link">
                    <?=$user->site;?>
                </a>
            </div>
        </div>
        <div class="center__intro d-flex">
            <img src="<?php echo base_url(); ?>img/red-back.png" alt="picture" class="center__intro-pic" />
            <div class="center__intro-text">
                <p class="center__meet">
                    Стоимость встречи
                </p>
                <p class="center__price">
                    <?=$user->meet_price;?>P
                </p>
            </div>
            <div class="center__right-wrap">
                <a href="#" class="center__star">
                    <img src="<?php echo base_url(); ?>img/small__yellow.png" alt="star" class="center__star-pic" />
                </a>
                <button class="center__btn btn">
                    <?=$user->status;?>
                </button>
            </div>
        </div>
    </div>
<?php } ?>