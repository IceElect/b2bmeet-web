<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Meet_model extends Default_model
{
    function __construct()
    {
        parent::__construct();
        $this->table = 'meets';
    }

    function getMeetByPayment($pId){
        $this->db->select('meets.*, users.token as client_token');
        $this->db->from($this->table);
        $this->db->join('users', 'users.id = meets.client_id', 'left');
        $this->db->where('meets.payment_id', $pId);
        $query = $this->db->get();
        // var_dump($this->db->last_query(),0);
        return $query->row();
    }
}