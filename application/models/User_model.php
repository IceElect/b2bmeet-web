<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class user_model extends Default_model
{
    function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }

    function save_user($aData, $action = 'add', $user_id = '')
    {
        if ($action == 'add') {
            $this->db->insert($this->table, $aData);
            $res = $this->db->insert_id();
        } else {
            if (!$user_id) return false;
            $this->db->where('id', $user_id);
            $res = $this->db->update($this->table, $aData);
        }
        if (!$res) {
            return false;
        } else {
            return $res;
        }
    }

    function update_user($user_id, $aData)
    {
        $this->db->where('id', $user_id);
        return $this->db->update($this->table, $aData);
    }

    public function getUserById($id)
    {
        $this->db->select('users.*, users.id as id, cities.id as city_id, company.id as company_id, posts.id as post_id, industries.id as industry_id');
        $this->db->from($this->table);
        $this->db->join('cities', 'cities.name = users.city', 'left');
        $this->db->join('company', 'company.name = users.company', 'left');
        $this->db->join('posts', 'posts.name = users.post', 'left');
        $this->db->join('industries', 'industries.name = users.industry', 'left');
        $this->db->where('users.id', $id);
        $query = $this->db->get();
        // var_dump($this->db->last_query(),0);
        return $query->row();
    }

    public function getUserByPhone($phone)
    {
        $this->db->select('users.*, users.id as id, cities.id as city_id, company.id as company_id, posts.id as post_id, industries.id as industry_id');
        $this->db->from($this->table);
        $this->db->join('cities', 'cities.name = users.city', 'left');
        $this->db->join('company', 'company.name = users.company', 'left');
        $this->db->join('posts', 'posts.name = users.post', 'left');
        $this->db->join('industries', 'industries.name = users.industry', 'left');
        $this->db->where('mob_phone', $phone);
        $query = $this->db->get();
        //dump($this->db->last_query(),0);
        return $query->row();
    }

    function getUsers($id, $search = false, $count = 999, $offset = 0)
    {

        $where = 'users.first_name <> "" AND cities.id <> "" ';
        if($id){
            $where .= "AND users.id != ".$id.' ';
        }
        if($search){
            $where .= 'AND (first_name like("%'.$search.'%") 
                OR last_name like("%'.$search.'%") 
                OR CONCAT(first_name, last_name) like("%'.$search.'%")
                OR CONCAT(first_name," ",last_name) like("%'.$search.'%")
                OR CONCAT(last_name, first_name) like("%'.$search.'%")
                OR CONCAT(last_name," ",first_name) like("%'.$search.'%"))';
        }

        $this->db->select('users.*, users.id as id, cities.id as city_id, company.id as company_id, posts.id as post_id, industries.id as industry_id, meets.user_id as meet_user_id, meets.client_id as meet_client_id, meets.date as meet_date, meets.text as meet_text, meets.status_id as meet_status, meets.id as meet_id, meets.rating as meet_rating, favorites.id as is_fav')
            ->from($this->table)
            ->join('cities', 'cities.name = users.city', 'left')
            ->join('company', 'company.name = users.company', 'left')
            ->join('posts', 'posts.name = users.post', 'left')
            ->join('industries', 'industries.name = users.industry', 'left')
            ->join('meets', '(meets.user_id = ' . $id . ' AND meets.client_id = users.id) OR (meets.client_id = ' . $id . ' AND meets.user_id = users.id)', 'left')
            ->join('favorites', 'favorites.user_id = ' . $id . ' AND favorites.fav_user_id = users.id', 'left')
            ->group_by('users.id')
            ->limit($count, $offset)
            ->where($where);
        $query = $this->db->get();
        // var_dump($this->db->last_query());
        return $query->result();
    }

    function getDataForExport($select = "*"){
        $this->db->select($select)
            ->from($this->table);

        $query = $this->db->get();
        // var_dump($this->db->last_query());
        return $query->result_array();
    }

    function saveDataFromImport($data){
        return $this->db->insert_batch($this->table, $data);
    }

    function exists($phone){
        $this->db->where('mob_phone', $phone);
        $this->db->from($this->table);
        return ($this->db->count_all_results() > 0);
    }
}