<?php 
/**
* @version      4.4.0 05.11.2013
* @author       MAXXmarketing GmbH
* @package      Jshopping
* @copyright    Copyright (C) 2010 webdesigner-profi.de. All rights reserved.
* @license      GNU/GPL
*/
defined('_JEXEC') or die('Restricted access');
 
        $jc = new JConfig();
        $_product = JSFactory::getTable('product', 'jshop');
        $_product->load($product->product_id);
        $model = JTable::getInstance('productShop', 'jshop');
        $back_value = $model->getBackValue($product->product_id, NULL);
        $model->setProduct($_product);
        $model->prepareView($back_value);
        $model->clearBackValue();
        $categories = $model->getCategories(1);
        $attributes = $model->getAttributes();
        $all_attr_values = $model->getAllAttrValues();

?>
<?php print $product->_tmp_var_start?>
<div class="product productitem_<?php print $product->product_id?>">
    <?php if ($product->image){?>
    <div class="image_block">
        <?php print $product->_tmp_var_image_block;?>
        <?php if ($product->label_id){?>
            <div class="product_label">
                <?php if ($product->_label_image){?>
                    <img src="<?php print $product->_label_image?>" alt="<?php print htmlspecialchars($product->_label_name)?>" />
                <?php }else{?>
                    <span class="label_name"><?php print $product->_label_name;?></span>
                <?php }?>
            </div>
        <?php }?>
        <a href="<?php print $product->product_link?>">
            <img class="jshop_img" src="<?php print $product->image?>" alt="<?php print htmlspecialchars($product->name);?>" title="<?php print htmlspecialchars($product->name);?>"  />
        </a>
        <?php print $product->_tmp_var_buttons;?>
        <a style="display: block;width: 220px;height: 30px;position: relative;line-height: 30px;text-align: center;text-transform: uppercase;color: #ffffff;font-family: 'Roboto Condensed Bold';font-size: 14px;cursor: pointer;background: #ff7901;margin: 10px auto 0px;" onclick="$('#submit-contact-form').click()" class="pod">Перезвоните мне</a>
    </div>
    <?php }?>
    <?php print $product->_tmp_var_bottom_foto;?>
    <div class = "prev-prod-info">
        <div class="name">
            <a href="<?php print $product->product_link?>"><?php print $product->name?></a>
            <?php if ($this->config->product_list_show_product_code){?><span class="jshop_code_prod">(<?php print _JSHOP_EAN?>: <span><?php print $product->product_ean;?></span>)</span><?php }?>
        </div>
        <div class="description">
            <?php print $product->short_description?>
        </div>
        <?php if ($product->manufacturer->name){?>
            <div class="manufacturer_name"><?php print _JSHOP_MANUFACTURER;?>: <span><?php print $product->manufacturer->name?></span></div>
        <?php }?>
        <?php if ($product->product_quantity <=0 && !$this->config->hide_text_product_not_available){?>
            <div class="not_available"><?php print _JSHOP_PRODUCT_NOT_AVAILABLE;?></div>
        <?php }?>

		<?php print $product->_tmp_var_bottom_old_price;?>
        <?php if ($product->product_price_default > 0 && $this->config->product_list_show_price_default){?>
            <div class="default_price"><?php print _JSHOP_DEFAULT_PRICE.": ";?><span><?php print formatprice($product->product_price_default)?></span></div>
        <?php }?>

        <?php if ($this->config->show_tax_in_product && $product->tax > 0){?>
            <span class="taxinfo"><?php print productTaxInfo($product->tax);?></span>
        <?php }?>
        <?php if ($this->config->show_plus_shipping_in_product){?>
            <span class="plusshippinginfo"><?php print sprintf(_JSHOP_PLUS_SHIPPING, $this->shippinginfo);?></span>
        <?php }?>
        <?php if ($product->basic_price_info['price_show']){?>
            <div class="base_price"><?php print _JSHOP_BASIC_PRICE?>: <?php if ($product->show_price_from) print _JSHOP_FROM;?> <span><?php print formatprice($product->basic_price_info['basic_price'])?> / <?php print $product->basic_price_info['name'];?></span></div>
        <?php }?>
        <?php if ($this->config->product_list_show_weight && $product->product_weight > 0){?>
            <div class="productweight"><?php print _JSHOP_WEIGHT?>: <span><?php print formatweight($product->product_weight)?></span></div>
        <?php }?>
        <?php if ($product->delivery_time != ''){?>
            <div class="deliverytime"><?php print _JSHOP_DELIVERY_TIME?>: <span><?php print $product->delivery_time?></span></div>
        <?php }?>
        <?php if (is_array($product->extra_field)){?>
            <div class="extra_fields">
            <?php foreach($product->extra_field as $extra_field){?>
                <div><?php print $extra_field['name'];?>: <span><?php print $extra_field['value']; ?></span></div>
            <?php }?>
            </div>
        <?php }?>

        <div class="review_mark">
            <div class="stars_no_active">
                <div class="stars_active" style="width:<?php print (25*$product->average_rating/$jshopConfig->rating_starparts);?>px"></div>
            </div>

            <div class="count_commentar">
                <?php print sprintf(_JSHOP_X_COMENTAR, $product->reviews_count);?>
            </div>
        </div>

        <?php if ($product->vendor){?>
            <div class="vendorinfo"><?php print _JSHOP_VENDOR?>: <a href="<?php print $product->vendor->products?>"><?php print $product->vendor->shop_name?></a></div>
        <?php }?>
        <?php if ($this->config->product_list_show_qty_stock){ ?>
            <div class="qty_in_stock"><?php print _JSHOP_QTY_IN_STOCK?>: <span><?php print sprintQtyInStock($product->qty_in_stock)?></span></div>
        <?php }?>

        <div class="prod-prev-price">
            <?php if ($product->product_old_price > 0){?>
                <div class="old_price"><?php if ($this->config->product_list_show_price_description) print _JSHOP_OLD_PRICE.": ";?><span><?php print formatprice($product->product_old_price)?></span></div>
            <?php }?>
            <?php if ($product->_display_price){?>
                <div class = "item_price">
                    <?php if ($this->config->product_list_show_price_description) print _JSHOP_PRICE.": ";?>
                    <?php if ($product->show_price_from) print _JSHOP_FROM." ";?>
                    <?php print formatprice($product->product_price); ?><?php print $product->_tmp_var_price_ext;?>
                </div>
            <?php }?>
            <?php print $product->_tmp_var_bottom_price;?>

            <?php print $product->_tmp_var_top_buttons;?>
            <div class="buttons">
            <?php if ($product->buy_link){?>
                <a class="btn btn-small button_buy" href="<?php print $product->buy_link?>"><?php print _JSHOP_BUY?></a>
            <?php }else{ ?>
                <form name="product" method="post" action="/allcategories/cart/add" enctype="multipart/form-data" autocomplete="off">
                    <?php if (count($attributes)) : ?>
                        <?php foreach($attributes as $attribut) : ?>
                            <span class="extra_fields_value" id='block_attr_sel_<?php print $attribut->attr_id?>' hidden>
                                <?php print $attribut->selects?>
                            </span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <input type="hidden" name="quantity" id="quantity" class="inputbox" value="1" />
                    <input type="hidden" name="to" id='to' value="cart" />
                    <input type="hidden" name="product_id" id="product_id" value="<?php print $product->product_id ?>" />
                    <input type="hidden" name="category_id" id="category_id" value="<?php print $categories[0] ?>" />
                    <button style="width: 100%;height: 30px;position: relative;line-height: 30px;text-align: center;text-transform: uppercase;color: #ffffff;font-family: 'Roboto Condensed Bold';font-size: 14px;cursor: pointer;background: #ff7901;margin: 8px auto 0px;" type="submit" class="btn btn-small button_buy" onclick="jQuery('#to').val('cart');"><?php print _JSHOP_BUY?></button>
                </form>
            <?php } ?>

                <!--<img src="/images/OPLATA.png" width="40" height="40" alt="" style="width: 80px; margin: 10px 0">-->
            </div>
            <?php print $product->_tmp_var_bottom_buttons;?>
        </div>
        <?php 
        //var_dump($jconfig);
        $db = mysql_pconnect($jc->host,$jc->user,$jc->password);
        mysql_select_db("systemgz_xn58s0b");
        $attr = mysql_query("SELECT * FROM v5yue_jshopping_products_free_attr WHERE product_id={$product->product_id} AND attr_id=1",$db);
        if ($attr) {
            $attr = mysql_num_rows($attr);
        }
        if ($attr) {

        ?>
        <div style="text-align:right;clear: both;padding-top:15px;">
            
            <span style="font-size:12px;border-bottom: 1px dashed #56a956;">При оплате набора скидка на аккумулятор 10%</span>
            <span style="color:#3bb32d;padding-left:10px;font-size:18px;fontn-weight:700;">
             <?php print formatprice($product->product_price*.9);?><?php print $product->_tmp_var_price_ext;?>
            </span>
        </div>
        <?php }?>
        
    </div>
</div>
<?php print $product->_tmp_var_end?>