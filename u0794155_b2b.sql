-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 29 2019 г., 03:00
-- Версия сервера: 5.7.24-27
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `u0794155_b2b`
--

-- --------------------------------------------------------

--
-- Структура таблицы `balance`
--

CREATE TABLE IF NOT EXISTS `balance` (
  `uid` int(10) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `locked` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `balance_history`
--

CREATE TABLE IF NOT EXISTS `balance_history` (
  `id` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `bill_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `old_balance` decimal(10,2) NOT NULL,
  `uid` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Москва'),
(2, 'Санкт-Петербург'),
(3, 'Волгоград'),
(4, 'Екатеринбург'),
(5, 'Казань'),
(6, 'Краснодар'),
(7, 'Нижний Новгород'),
(8, 'Пермь'),
(9, 'Ростов-на-Дону'),
(10, 'Самара'),
(11, 'Уфа'),
(12, 'Челябинск'),
(13, 'Балашиха'),
(14, 'Воскресенск'),
(15, 'Дмитров'),
(16, 'Домодедово'),
(17, 'Железнодорожный'),
(18, 'Клин'),
(19, 'Коломна'),
(20, 'Коломна'),
(21, 'Красногорск'),
(22, 'Лобня'),
(23, 'Люберцы'),
(24, 'Мытищи'),
(25, 'Ногинск'),
(26, 'Одинцово'),
(27, 'Одинцово'),
(28, 'Подольск'),
(29, 'Пушкино'),
(30, 'Раменское'),
(31, 'Раменское'),
(32, 'Раменское'),
(33, 'Химки'),
(34, 'Чехов'),
(35, 'Чехов'),
(36, 'Электросталь'),
(37, 'Московская область'),
(38, 'Омск'),
(39, 'Хабаровск'),
(40, 'Ярославль'),
(41, 'Новосибирск'),
(42, 'Красноярск'),
(43, 'Крым'),
(44, 'Севастополь');

-- --------------------------------------------------------

--
-- Структура таблицы `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL,
  `inn` varchar(12) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `company`
--

INSERT INTO `company` (`id`, `inn`, `name`) VALUES
(1, '554', 'тест'),
(2, '321', 'cgvccc'),
(3, '', 'таксре'),
(4, '45677894457', 'таксре');

-- --------------------------------------------------------

--
-- Структура таблицы `favorites`
--

CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fav_user_id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `fav_user_id`, `date`) VALUES
(9, 2, 5, '2019-09-18 08:53:18'),
(10, 11, 5, '2019-09-18 20:25:05'),
(11, 105, 100, '2019-09-25 22:01:30');

-- --------------------------------------------------------

--
-- Структура таблицы `industries`
--

CREATE TABLE IF NOT EXISTS `industries` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `industries`
--

INSERT INTO `industries` (`id`, `name`) VALUES
(1, 'Автомобильный бизнес'),
(2, 'Гостиницы, рестораны, общепит, кейтеринг'),
(3, 'Государственные организации'),
(4, 'Добывающая отрасль'),
(5, 'ЖКХ'),
(6, 'Информационные технологии, системная интеграция, интернет'),
(7, 'Искусство, культура'),
(8, 'Лесная промышленность, деревообработка'),
(9, 'Медицина, фармацевтика, аптеки'),
(10, 'Металлургия, металлообработка'),
(11, 'Нефть и газ'),
(12, 'Образовательные учреждения'),
(13, 'Общественная деятельность, партии, благотворительность, НКО'),
(14, 'Перевозки, логистика, склад, ВЭД'),
(15, 'Продукты питания'),
(16, 'Промышленное оборудование, техника, станки и комплектующие'),
(17, 'Розничная торговля'),
(18, 'СМИ, маркетинг, реклама, BTL, PR, дизайн, продюсирование'),
(19, ''),
(20, 'Сельское хозяйство'),
(21, 'Строительство, недвижимость, эксплуатация, проектирование'),
(22, 'Телекоммуникации, связь'),
(23, 'Товары народного потребления (непищевые)'),
(24, 'Тяжелое машиностроение'),
(25, 'Управление многопрофильными активами'),
(26, 'Услуги для бизнеса'),
(27, 'Услуги для населения'),
(28, 'Финансовый сектор'),
(29, 'Химическое производство, удобрения'),
(30, 'Электроника, приборостроение, бытовая техника, компьютеры и оргт'),
(31, 'Энергетика'),
(32, 'Оптовая торговля');

-- --------------------------------------------------------

--
-- Структура таблицы `meets`
--

CREATE TABLE IF NOT EXISTS `meets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `payment_id` varchar(32) NOT NULL,
  `rating` int(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meets`
--

INSERT INTO `meets` (`id`, `user_id`, `client_id`, `text`, `status_id`, `payment_id`, `rating`, `date`) VALUES
(49, 7, 2, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 4, '69', 0, '2019-09-11'),
(55, 2, 11, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 3, '', 0, '2019-09-18'),
(56, 5, 11, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 2, '', 0, '2019-09-18'),
(57, 7, 11, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 4, '71', 0, '2019-09-18'),
(58, 100, 105, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 2, '', 0, '2019-09-25'),
(59, 7, 106, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 2, '', 0, '2019-09-25'),
(60, 100, 106, 'С вами хотят встретиться по делу, оплатят за ваше время, b2bmeet.ru', 2, '', 0, '2019-09-25');

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `name`) VALUES
(2, 'cgv'),
(4, 'test'),
(3, 'vvvbv'),
(1, 'Программист'),
(5, 'царь');

-- --------------------------------------------------------

--
-- Структура таблицы `robokassa_history`
--

CREATE TABLE IF NOT EXISTS `robokassa_history` (
  `id` bigint(20) NOT NULL,
  `uid` int(10) NOT NULL,
  `bid` bigint(20) DEFAULT NULL,
  `sum` decimal(10,2) DEFAULT NULL,
  `bill_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `currency` text CHARACTER SET utf8 COLLATE utf8_bin,
  `account` text CHARACTER SET utf8 COLLATE utf8_bin,
  `cur_sum` decimal(10,2) DEFAULT NULL,
  `our_currency` text CHARACTER SET utf8 COLLATE utf8_bin,
  `paid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `robokassa_history`
--

INSERT INTO `robokassa_history` (`id`, `uid`, `bid`, `sum`, `bill_date`, `currency`, `account`, `cur_sum`, `our_currency`, `paid`) VALUES
(30, 24, NULL, NULL, '2019-09-10 18:31:24', NULL, NULL, NULL, NULL, 0),
(31, 24, NULL, NULL, '2019-09-10 18:31:38', NULL, NULL, NULL, NULL, 0),
(32, 24, NULL, NULL, '2019-09-10 18:31:38', NULL, NULL, NULL, NULL, 0),
(33, 24, NULL, NULL, '2019-09-10 18:31:39', NULL, NULL, NULL, NULL, 0),
(34, 24, NULL, NULL, '2019-09-10 18:31:39', NULL, NULL, NULL, NULL, 0),
(35, 24, NULL, NULL, '2019-09-10 18:31:40', NULL, NULL, NULL, NULL, 0),
(36, 24, NULL, NULL, '2019-09-10 18:31:41', NULL, NULL, NULL, NULL, 0),
(37, 24, NULL, NULL, '2019-09-10 18:31:41', NULL, NULL, NULL, NULL, 0),
(38, 24, NULL, NULL, '2019-09-10 18:31:42', NULL, NULL, NULL, NULL, 0),
(39, 24, NULL, NULL, '2019-09-10 18:31:43', NULL, NULL, NULL, NULL, 0),
(40, 24, NULL, NULL, '2019-09-10 18:31:43', NULL, NULL, NULL, NULL, 0),
(41, 24, NULL, NULL, '2019-09-10 19:13:25', NULL, NULL, NULL, NULL, 0),
(42, 24, NULL, NULL, '2019-09-10 19:19:27', NULL, NULL, NULL, NULL, 0),
(43, 24, NULL, NULL, '2019-09-10 19:20:46', NULL, NULL, NULL, NULL, 0),
(44, 24, NULL, NULL, '2019-09-10 19:21:27', NULL, NULL, NULL, NULL, 0),
(45, 24, NULL, NULL, '2019-09-10 19:41:07', NULL, NULL, NULL, NULL, 0),
(46, 24, NULL, NULL, '2019-09-10 19:41:25', NULL, NULL, NULL, NULL, 0),
(47, 24, NULL, NULL, '2019-09-10 19:44:38', NULL, NULL, NULL, NULL, 0),
(48, 24, NULL, NULL, '2019-09-10 19:46:45', NULL, NULL, NULL, NULL, 0),
(49, 24, NULL, NULL, '2019-09-10 19:47:59', NULL, NULL, NULL, NULL, 0),
(50, 24, NULL, NULL, '2019-09-10 19:48:18', NULL, NULL, NULL, NULL, 0),
(51, 25, NULL, NULL, '2019-09-10 19:49:07', NULL, NULL, NULL, NULL, 0),
(52, 23, NULL, NULL, '2019-09-10 23:31:18', NULL, NULL, NULL, NULL, 0),
(53, 26, NULL, NULL, '2019-09-10 23:35:01', NULL, NULL, NULL, NULL, 0),
(54, 27, NULL, NULL, '2019-09-10 23:36:29', NULL, NULL, NULL, NULL, 0),
(55, 27, NULL, NULL, '2019-09-10 23:47:11', NULL, NULL, NULL, NULL, 0),
(56, 27, NULL, NULL, '2019-09-11 00:34:09', NULL, NULL, NULL, NULL, 0),
(57, 27, NULL, NULL, '2019-09-11 00:34:44', NULL, NULL, NULL, NULL, 0),
(58, 27, NULL, NULL, '2019-09-11 01:15:39', NULL, NULL, NULL, NULL, 0),
(59, 27, NULL, NULL, '2019-09-11 04:30:29', NULL, NULL, NULL, NULL, 0),
(60, 28, NULL, NULL, '2019-09-11 04:33:58', NULL, NULL, NULL, NULL, 0),
(61, 29, NULL, NULL, '2019-09-11 04:34:43', NULL, NULL, NULL, NULL, 0),
(62, 40, NULL, NULL, '2019-09-11 05:24:58', NULL, NULL, NULL, NULL, 0),
(63, 44, NULL, NULL, '2019-09-11 05:33:33', NULL, NULL, NULL, NULL, 0),
(64, 45, NULL, NULL, '2019-09-11 05:36:47', NULL, NULL, NULL, NULL, 0),
(65, 46, NULL, NULL, '2019-09-11 05:37:31', NULL, NULL, NULL, NULL, 0),
(66, 47, NULL, NULL, '2019-09-11 05:39:36', NULL, NULL, NULL, NULL, 0),
(67, 48, NULL, NULL, '2019-09-11 06:41:01', NULL, NULL, NULL, NULL, 0),
(68, 48, NULL, NULL, '2019-09-11 06:42:59', NULL, NULL, NULL, NULL, 0),
(69, 49, NULL, NULL, '2019-09-11 06:52:30', NULL, NULL, NULL, NULL, 0),
(70, 57, NULL, NULL, '2019-09-18 17:27:49', NULL, NULL, NULL, NULL, 0),
(71, 57, NULL, NULL, '2019-09-18 17:28:47', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'tesdt1'),
(2, 'test2'),
(3, 'мвпр'),
(4, 'оценка'),
(5, 'юридические услуги');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `second_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `city` varchar(64) NOT NULL,
  `post` varchar(64) NOT NULL,
  `company` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `inn` varchar(12) NOT NULL,
  `industry` varchar(255) NOT NULL,
  `meet_price` int(11) NOT NULL,
  `mob_phone` varchar(32) NOT NULL,
  `site` varchar(64) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `code` int(4) NOT NULL,
  `token` varchar(255) NOT NULL,
  `partner_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `first_name`, `second_name`, `last_name`, `city`, `post`, `company`, `email`, `inn`, `industry`, `meet_price`, `mob_phone`, `site`, `tags`, `code`, `token`, `partner_id`) VALUES
(2, 'Алексей', 'Алексеевич', 'Сафронов', 'Москва', 'test', 'тест', 'a@slto.ru', '554', 'Веб', 2000, '+79779486965', 'test', '[{"id":1,"name":"test2"},{"id":1,"name":"tesdt1"}]', 0, 'eF0TrUFFsZo:APA91bExv0fhpCaenMUuYnHw3Oc36m69wsrmU2KLeiEiK8-QVepzb6oEZGf9SEX_cnYnkvB1mzgQkbYMDKGhK5rjLXUxC4qLk4_RUPuz0CJpmIbyUrJf22bnH6RhM8F-2Y5knHujl4tm', 0),
(3, 'Иван', 'Иванович', 'Иванов', '', '', '', '', '', '', 0, '+79109295271', '', '', 0, '', 0),
(5, 'Тест', 'Тест', 'Темт', 'Москва', 'vvvbv', 'тест', 'fhfd@ggff.ru', '554', 'Веб', 200, '+79853050152', '', '', 0, 'cvZZ53dV9fc:APA91bH0wHJ2FwvU5Na2qx3n6e4Z8x6WE6y5AZoF9l8fsiR9pSzeuOXdR0PiR7ZxD6RgmQ5_ECUIzSIGrgDaTAQCz_RhU3HIJ5KQ0SOxfVsW4js1Gakz_qHKnKMkeSvt-H7T6t81MYP3', 0),
(7, 'Artemy', 'Gorbunov', '', 'Москва', 'Программист', 'cgvccc', 'artemygorbunov@gmail.com', '321', 'Веб', 10, '1', 'www.b2bmeet.ru', '[{"id":1,"name":""}]', 0, 'f4sGvNQQB30:APA91bG-8PQXK3UIMdmS07DJBdo0Vme6EeMKGDgZEfdDj0bFlVH3JNFG3R4mqW0XzC9X6b1j6uEE6gcKNxzhpj9cn5O1CHJmF67GjxEv8HhIPIBjLAjeD81O31fZIhgOETKO65rCGVK9', 0),
(8, 'Вася', 'Васин', 'Пупкин', '', '', '', '', '', '', 0, '+79684759048', '', '', 0, 'eUDxpjZANwk:APA91bFwJPFwlGbgxV3n2gNfxf_E7z6iMUT0APBDPGdiTdJM7ybSW4hgvfIt9WcDI4tNsmFtlZAv6C5V2PqIGkFO7YeGhu6DYx_3WWf3KavCJrblMc78NaDP6VrZuBl_EWN-mJ0T-jLA', 0),
(9, 'Маша', 'Катина', 'Викторовна', '', '', '', '', '', '', 0, '+79674759048', '', '[]', 0, 'eUDxpjZANwk:APA91bFwJPFwlGbgxV3n2gNfxf_E7z6iMUT0APBDPGdiTdJM7ybSW4hgvfIt9WcDI4tNsmFtlZAv6C5V2PqIGkFO7YeGhu6DYx_3WWf3KavCJrblMc78NaDP6VrZuBl_EWN-mJ0T-jLA', 0),
(11, 'Артттт', 'Прлдт', '', 'Москва', 'Программист', 'тест', 'atuhfg@gjj.com', '554', 'ЖКХ', 10, '2', '', '[{"id":1,"name":""}]', 0, 'f4sGvNQQB30:APA91bG-8PQXK3UIMdmS07DJBdo0Vme6EeMKGDgZEfdDj0bFlVH3JNFG3R4mqW0XzC9X6b1j6uEE6gcKNxzhpj9cn5O1CHJmF67GjxEv8HhIPIBjLAjeD81O31fZIhgOETKO65rCGVK9', 0),
(12, '', '', '', '', '', '', '', '', '', 0, '+79165151134', '', '[]', 0, 'f5VeenuluK4:APA91bHJiuBrZwXMRelY__GRBd7PT7L7oO_e9EPB0YsegIpJAi6J2CHIEBdoFOPxJgL-74mQcVokZPeEImxWkR7fyZOX_smm-VlsySA6ECnGppAsKPtApJCy0XLXtmiv5Py4EJz-ty9S', 0),
(13, '', '', '', '', '', '', '', '', '', 0, '+7555698', '', '[]', 0, 'dz21pLX5usk:APA91bEdNRIbJ7Yvt6VBSPd4cb_oHKle_jUJHpGC6EsijJsQtmNLbVR1UxPwRAne3AG0oRSl3SHDFENt254JXAW2zKb83BrGzgrgrv7ZQcPXCVZ8ZqsE4YBIsw7ztYU7SC_ZGRLZzOzp', 0),
(14, '', '', '', '', '', '', '', '', '', 0, '+7915475855', '', '[]', 0, 'f9eGNJgMATw:APA91bE8XYVYPhQ8FN-GwhTPV3Bm15ALnqTT2V44fVoDcDRZiOBm4WdgpYbSNbUy57gdl9mErrGCqI-D80pehRp_SY3bmE_TH-ZXOkntFLEQ8mVFFq_mkKd4vYQdh8Q0ImB_VLacc_Be', 0),
(99, 'Сергей', 'Шемякин', 'Андреевич', 'Москва', 'Исполнительный директор', 'курорт', 'inigrey@gmail.com', '', '5', 10, '380636436113', 'google.com', '', 0, '', 0),
(100, 'Артемий', 'Горбунов', 'Викторович', 'Москва', 'Владелец / Генеральный директор', 'ИП Горбунов А.В.', 'artemygorbunov@gmail.com', '', '5', 10000, '79250050530', '', '', 0, '', 0),
(101, 'Николай', 'Жигайлов', 'Анатольевич', 'Москва', 'Генеральный Директор', 'ООО "Глобал-Торг"', 'nikolauskas81@gmail.com', '', '0', 5000, '79629168062', '', '', 0, '', 0),
(102, 'Леонид', 'Заболотный', 'Иванович', 'Москва', 'Коммерческий директор', 'ООО "Леовит Нутрио", г. Москва. Производитель лечебно-профилакти', 'zableo@mail.ru', '', '0', 5000, '79163146389', '', '', 0, '', 0),
(103, 'Виктор', 'Устинов', 'Викторович', 'Москва', 'старший научный сотрудник', 'ФГБОУ ВПО "Государственный морской университет имени адмирала Ф.', 'ustinovnvrsk@mail.ru', '', '0', 5000, '79887629243', '', '', 0, '', 0),
(104, 'Эдуард', 'Малежик', 'Анатольевич', 'Москва', 'Первый заместитель генерального директора', 'ООО "Управляющая компания "Партнёрство"', '5002718@mail.ru', '', '0', 5000, '79255002718', '', '', 0, '', 0),
(105, '', '', '', '', '', '', '', '', '', 0, '+79250050530', '', '[]', 0, 'fBRvS_beVsg:APA91bE3xSMvvzovvcjE_sRRHKa0w8svVw1ILevKDiGBbtS3XGD0rHLtUN_PilMax7pZFtIDxeRN1JxYCq1yN2UIEq3wAXBTsnwqSks9mvVUt_iRk-cSq8x31C-ZKOIZq4lSysSNQCqI', 0),
(106, '', '', '', '', '', '', '', '', '', 0, '+79153021973', '', '[]', 0, 'dE6-NU4wvEg:APA91bFsjR-HVKyv9q7Dix-FgWlDRiMTow-GYU60SKPvojp5iZaqdnYuXYXBw8eOTAOSaLdpI_x7OajXYXnqDHdgq9zMFLpYWUz8vTSq97Fjk4wDvRgwArMoE9MR9sHaFEdrSRLpbroE', 0),
(107, '1', 'Й', 'Й', 'Санкт-Петербург', 'Программист', 'таксре', 'й@ар.рф', '', '', 1, '11', 'п', '[{"id":1,"name":""}]', 0, '', 0),
(110, '1', 'Й', 'Й', 'Санкт-Петербург', 'Программист', 'таксре', 'й@ар.рф', '', '', 1, '111', 'п', '[{"id":1,"name":""}]', 0, '', 0),
(112, '1', 'Й', 'Й', 'Санкт-Петербург', 'Программист', 'таксре', 'й@ар.рф', '', '', 1, '111111', 'п', '[{"id":1,"name":""}]', 0, '', 0),
(115, '1', 'Й', 'Й', 'Санкт-Петербург', 'Программист', 'таксре', 'й@ар.рф', '', '', 1, '111234234', 'п', '[{"id":1,"name":""}]', 0, '', 0),
(116, '1', 'Й', 'Й', 'Санкт-Петербург', 'Программист', 'таксре', 'й@ар.рф', '', '', 1, '1115545655', 'п', '[{"id":1,"name":""}]', 0, '', 0),
(117, 'Партнер', 'Партнер', 'Партнер', 'Санкт-Петербург', 'Программист', 'cgvccc', 'иаа', '321', '', 5000, '56589', '', '[{"id":1,"name":""}]', 0, '', 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`uid`);

--
-- Индексы таблицы `balance_history`
--
ALTER TABLE `balance_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `meets`
--
ALTER TABLE `meets`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_2` (`name`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `robokassa_history`
--
ALTER TABLE `robokassa_history`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mob_phone` (`mob_phone`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `balance_history`
--
ALTER TABLE `balance_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблицы `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `industries`
--
ALTER TABLE `industries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `meets`
--
ALTER TABLE `meets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `robokassa_history`
--
ALTER TABLE `robokassa_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
